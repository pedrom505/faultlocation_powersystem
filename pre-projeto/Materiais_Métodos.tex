\chapter{Materiais e métodos} \label{section:materiais_metodos}

\section{Metodologia}

\par
A dissertação proposta neste documento visa desenvolver um algoritmo utilizando técnicas de análise de dados e de aprendizado de máquina para detecção e localização de eventos na rede elétrica que possam causar danos estruturais na rede e interrupções ou instabilidade no fornecimento aos centros consumidores.
\par
Para o desenvolvimento desta dissertação, um grande volume de dados será necessário para que os algoritmos de aprendizado de máquina sejam treinados apropriadamente, entretanto, no presente momento há uma escassez de dados provenientes das PMUs espalhadas pelo sistema elétrico real disponível. Para contornar esta limitação no volume de dados, inicialmente todos os testes do algoritmo proposto serão efetuados em um ambiente simulado utilizando como referência o sistema de 39 barras do IEEE apresentado na sessão \ref{section:IEEE_39_barras}.
\par
O processo de simulação do sistema de 39 barras do IEEE será realizado através da \textit{Power system Toolbox} (PST) \cite{PST} apresentada na sessão \ref{section:PST}. Após a geração dos dados simulados, todas as análises dos sinais serão feitas utilizando a linguagem de programação Python.
\par
Levando em conta a robustez dos sistemas elétricos modernos, o volume de dados de falhas disponíveis na prática são muito baixo dificultando o uso de dados de falha no processo de treinamento dos algoritmos de aprendizado de máquina. Por este motivo a presente dissertação irá desenvolver uma abordagem capaz de efetuar o processo de detecção e localização de faltas utilizando somente dados de operação normal do sistema.
\par
A disposição das PMUs no sistema elétrico será um dos itens de estudo desta dissertação. Inicialmente serão posicionadas PMUs em todas as barras do sistema de teste e este número será reduzido durante os testes para se avaliar o impacto da redução das PMUs, assim como seu posicionamento, no resultado final da localização das falhas.
\par
Será avaliado o uso da técnica R3LS apresentada na sessão \ref{section:R3LS} como algoritmo de detecção de falhas para sistema de teste proposto e os resultados obtidos para este algoritmo serão comparado com outras técnicas de deteção de anomalias a serem definidas.
\par
Com o conjunto de dados devidamente estruturado e o algoritmo de detecção de falhas funcionando, será feito o treinamento de um algoritmo de aprendizado de máquina com a finalidade de localizar a barra aonde está ocorrendo a falta que foi detetada. 

\subsection{Sistema de 39 barras do IEEE} \label{section:IEEE_39_barras}
\par
O sistema de 39 barras do IEEE, comumente conhecido como \textit{New England IEEE 39-Bus System}, será utilizado neste dissertação como modelo de base em um ambiente simulado, onde o comportamento deste sistema operando dinamicamente será analisado para a implementação do sistema de localização de faltas proposto. Todos os parâmetros deste sistema são apresentados e especificados em um artigo por \cite{39bars_system}.
\par
O diagrama deste sistema é apresentado na figura \ref{image:39bars_diagram} 

\begin{figure} [!ht]
    \centering
    \caption{Sistema de 39 barras do IEEE}
    \includegraphics[width=16cm]{39Bars_diagram.png}
    FONTE: \cite{39bars_system}
    \label{image:39bars_diagram}
\end{figure}  

\par
Para efetuar a simulação dinâmica do sistema em questão, a \textit{toolbox} PST \cite{PST} necessita dos parâmetros de operação do sistema e estes dados serão apresentados nas próximas sessões.

\subsubsection{Dados das barras}
\par
A tabela \ref{table:39bars_bars} apresenta os dados de potencia e tensão do sistema de 39 barras do IEEE, aonde os dados apresentados estão na base de MVA. Note que o gerador 2 será considerado como o nó \textit{swing} e o gerador 1 representa um aglomerado de um grande número de geradores.

\begin{table} [!ht]
    \centering
    \caption{Dados de barras do sistema de 39 barras do IEEE}
    \includegraphics[width=11cm]{39bars_bars.png}
    \par FONTE: \cite{39bars_parameters_matlab}.
    \label{table:39bars_bars}
\end{table} 

\subsubsection{Dados das linhas}
\par
A tabela \ref{table:39bars_lines} apresenta os dados de linha do sistema de 39 barras do IEEE, aonde os dados apresentados estão na base de MVA a 60Hz.

\begin{table} [!ht]
    \centering
    \caption{Dados de linha do sistema de 39 barras do IEEE}
    \includegraphics[width=7cm]{39bars_lines.png}
    \par FONTE: \cite{39bars_parameters_matlab}.
    \label{table:39bars_lines}
\end{table} 

\subsubsection{Modelo dos geradores}
\par
O sistema de 39 barras do IEEE é composto por 10 geradores e todos eles serão representados por um modelo de 6º ordem estimado por \cite{39bars_parameters_anatem} para que sejam simulados através da \textit{toolbox} PST \cite{PST}. Os parâmetros estimados dos geradores serão apresentados nas tabelas \ref{table:39bars_generator_parameter_1} e \ref{table:39bars_generator_parameter_2} a seguir:

\begin{table} [!ht]
    \centering
    \caption{Parâmetros dos geradores}
    \includegraphics[width=16cm]{39Bars_generator_parameters_1.png}
    \par FONTE: \cite{39bars_parameters_anatem}.
    \label{table:39bars_generator_parameter_1}
\end{table} 

\begin{table} [!ht]
    \centering
    \caption{Parâmetros estimados dos geradores}
    \includegraphics[width=7cm]{39Bars_generator_parameters_2.png}
    \par FONTE: \cite{39bars_parameters_anatem}
    \label{table:39bars_generator_parameter_2}
\end{table} 


\subsubsection{Modelo dos controladores}
\par
Todos os geradores do sistema são equipados com AVRs (\textit{Automatic Voltage Regulators}) e PSSs \textit{Power System Stabilizers}). Estes geradores utilizam o mesmo modelo de controlador se diferenciando apenas pelos seus correspondentes parâmetros de acordo com a especificação do sistema apresentada por \cite{39bars_system}

\subsubsection{Estabilizadores de sistema de energia (PSS)}
\par
Cada gerador do sistema esta relacionado a um  PSS (do tipo $\delta$ e $\omega$ com dois blocos de mudança de fase. O PSS utilizado na simulação deste sistema foi especificado por \cite{39bars_parameters_matlab} e o modelo deste PSS é apresentado na figura \ref{figure:39bars_pss_model} abaixo:

\begin{figure} [!ht]
    \centering
    \caption{Diagrama de blocos do PSS}
    \includegraphics[width=14cm]{39bars_pss_model.png}
    \par FONTE: \cite{39bars_parameters_matlab}.
    \label{figure:39bars_pss_model}
\end{figure} 

\par
Os parâmetros do PSS apresentados no modelo da figura \ref{figure:39bars_pss_model} são especificados de acordo com a tabela \ref{table:39bars_pss_parameters}.

\begin{table} [!ht]
    \centering
    \caption{Parâmetros dos PSS de cada gerador}
    \includegraphics[width=16cm]{39bars_pss_parameters.png}
    \par FONTE: \cite{39bars_parameters_matlab}.
    \label{table:39bars_pss_parameters}
\end{table} 

\subsubsection{Modelo do regulador automático de tensão (AVR)}
\par
Todos os geradores do sistema são equipados com um AVR. O modelo deste controlador é apresentado na figura \ref{figure:39bars_avr_model} abaixo:

\begin{figure} [!ht]
    \centering
    \caption{Diagrama de blocos do AVR}
    \includegraphics[width=14cm]{39bars_avr_model.png}
    \par FONTE: \cite{39bars_parameters_anatem}.
    \label{figure:39bars_avr_model}
\end{figure} 

\par
Os parâmetros do AVR são definidos conforme apresentado na tabela \ref{table:39bars_avr_parameters} abaixo:

\begin{table} [!ht]
    \centering
    \caption{Parâmetros dos AVR de cada gerador}
    \includegraphics[width=14cm]{39bars_avr_parameters.png}
    \par FONTE: \cite{39bars_parameters_matlab}.
    \label{table:39bars_avr_parameters}
\end{table} 


\subsection{PST - Power system Toolbox} \label{section:PST}
\par
Para efetuar as simulações do sistema de teste proposto está sendo utilizada a \textit{toolbox} feita para MATLAB denominada \textit{Power system Toolbox} (PST) \cite{PST}.
\par
Esta \textit{toolbox} disponibiliza um conjunto de ferramentas que possibilitam a simulação dinâmica de um sistema elétrico, utilizando como entrada algumas matrizes que especificam os parâmetros de operação do sistema como barras, linhas, geradores e controladores. Em conjunto com as matrizes de parâmetros, a \textit{toolbox} necessita de uma matriz de instruções que irá ditar o comportamento do sistema ao longo da simulação. A tabela \ref{table:Matriz_instrucoes_PST} apresenta a matriz de instruções:

\begin{table} [!ht]
    \centering
    \caption{Matriz de instruções da PST}
    \includegraphics[width=16cm]{Matriz_instrucoes_PST.png}
    \par FONTE: \cite{PST}.
    \label{table:Matriz_instrucoes_PST}
\end{table}

\par
Os elementos que compões a matriz de instruções são:

\begin{itemize}
    \item Ti - Instante de inicio da simulação;
    \item Tif - Instante de ocorrência da falta;
    \item Tff - Instante de fim da falta;
    \item Tfd - Instante de fim da falta na barra distante;
    \item Tt - Instante de mudança do tempo de amostragem;
    \item Tf - Instante de fim da simulação;
    \item Br - número da barra de referência;
    \item Bd - número da barra distante;
    \item Z0 - Impedância de falta de sequência zero;
    \item Zn - Impedância de falta de sequência negativa;
    \item Ft - Tipo de falta:
    \begin{itemize}
        \item 0 - Falta trifásica;
        \item 1 - Falta fase-terra;
        \item 2 - Falta fase-fase-terra;
        \item 3 - Falta fase-fase;
        \item 4 - Contingência de linha;
        \item 5 - Contingência de carga;
        \item 6 - Nenhuma falta.
    \end{itemize}
    \item t - Período de amostragem.
\end{itemize}

\par
Como resultado da simulação, o script de simulação desenvolvido irá entregar os seguintes dados:
\begin{itemize}
    \item Módulo de tensão;
    \item Ângulo da tensão;
    \item Módulo de corrente;
    \item Ângulo da corrente;
    \item Frequência.
\end{itemize}
\par
A imagem \ref{image:39Bars_Example} apresenta o resultado de uma simulação realizada no MATLAB utilizando a PST aonde o eixo X representa as amostras obtidas para cada barra do sistema ao longo do tempo, o eixo Y representa as 39 barras do sistema e o eixo Z é representa o módulo da tensão nas barras. Esta simulação apresenta o comportamento do sistema no momento em que ocorre uma falta de curto-circuito fase-fase na barra 3.

\begin{figure} [!ht]
    \centering
    \caption{Módulo de tensão nas 39 barras do sistema durante um curto-circuito fase-fase na barra 3}
    \includegraphics[width=16cm]{39Bars_Example.png}
    FONTE: O Autor (2021)
    \label{image:39Bars_Example}
\end{figure}  


\subsection{R3LS - \textit{Robust Recursive Least Squares}} \label{section:R3LS}
\par
O R3LS (\textit{Regularized Robust Recursive Least Squares}) proposto por \cite{R3LS} é um algoritmo recursivo destinado a identificação de sistemas baseado nos dados de medição deste sistema em condições de operação normal aonde os dados medidos apresentam um estado estacionário, sem perturbações significativas ou transitória aonde os dados medidos apresentam a resposta a uma perturbação repentina que ocorreu no sistema.
\par
Será avaliada a possibilidade de uso do algoritmo R3LS como algoritmo de detecção de faltas no sistema de teste.
\par
Para a abordagem proposta por esta dissertação, o erro de estimativa gerado pelo R3LS será encarado como a saída quantificada do algoritmo de detecção de anomalias e esta saída por sua vez será utilizada pelo algoritmo de aprendizado de máquina responsável pela localização da falta no sistema.