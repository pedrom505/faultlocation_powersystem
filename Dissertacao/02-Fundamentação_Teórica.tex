\chapter{Fundamentação teórica} \label{section:fundamentacao_teorica}

Neste capítulo serão abordados os fundamentos dos principais elementos abordados neste trabalho, sendo eles o software de simulação Anatem/Anarede, dispositivo de medição sincrofasorial e maquina de vetor de suporte. Para estes elementos será apresentando seus históricos, principais funcionalidades, características e conexão com a proposta do trabalho. Além dos temas acima apontados, serão apresentado os conceitos relativos a etapas
de preparação do sinal.

\section{Localização de faltas}

\par
O processo de restauração de SEPs de grande escala requer métodos para localizar e isolar falhas que venham a ocorrer na linha com eficiência \cite{RTFLLPPCNN}.

\par
Conforme apresentado por \textcite{RTFLLPPCNN}, técnicas de localização de falhas são utilizados para apontar em que ponto especifico do sistema esta ocorrendo uma falta. Os métodos utilizados até o momento neste contexto podem ser categorizados em duas classes. 1) Método convencional e 2) algoritmos de wide-area. 

\par
Os métodos convencionais costumam ser baseados impedância, ondas viajantes e métodos utilizando inteligência artificial. 
\par
Métodos baseados em impedância calculam a corrente de falta com base na medida da tensão e na impedância do barramento que é um valor conhecido, para determinar a distancia entre o ponto de falta e o ponto de medição. Este tipo de método assume que a carga é estática e costuma ser bastante dependente dos parâmetros das linhas \cite{AFLMODNWLNOP}.

\par
Os métodos baseado em ondas viajantes vem sendo aplicado em SEPs desde 1950, relacionando a diferença de tempo do tempo de propagação de uma onda no barramento para determinar a localização da falha. Este método requer alta precisão na sincronização das medições e alta taxa de amostragem \cite{AFLMODNWLNOP}.

\par
Os métodos baseados em inteligência artificial, treinam algoritmos como redes neurais ou SVM com características extraídas do sistema. Geralmente os dados brutos do sistema precisam ser previamente processados, e após este pré-processamento os dados são tratados como características do sistema.

\par
Nos métodos de wide-area, primeiramente a região com problema é localizada (sendo esta região uma linha especifica do sistema ou uma região aproximada) e então um segundo método é utilizado para determinar a posição exata \cite{WAMS_PMU}.

\section{Dispositivo de medição sincrofasorial} \label{section:Sistema_de_medição_sincrofasorial}

\par
A medições das grandezas elétricas dos SEPs foram inicialmente utilizadas como entrada para métodos de estimação de estados estáticos e estes estimadores foram projetados para prover, em tempo real, o estado de operação atual do sistema, afim de dar segurança aos operadores em suas tomadas de decisão, porem, era reconhecido que este tipo de tecnologia não possibilitaria medições simultâneas em uma alta taxa de amostragem em diferentes pontos de um SEP com extensões continentais \cite{SPMHO}. 
\par
Com o intuito de aumentar o monitoramento dos sistemas elétricos em tempo real e melhorar os sistemas de medição, em meados dos anos de 1980 foi introduzido o conceito de medição de fasor sincronizada com o advento das Unidades de medição sincrofasorial, do ingles \criarsigla*{PMU}{\textit{Phasor Measurement Unit}}. Desde então, o uso de PMUs no processo de medição de grandezas elétricas em um SEP de grande porte tem recebido grande atenção dos pesquisadores relacionados ao campo.
\par
O desenvolvimento das tecnologias por trás das PMUs foi conduzido inicialmente pela "Virginia Tech" por diversos patrocinadores ao longo de vários anos, aonde os principais interessados/investidores era o departamento de energia dos Estados unidos, os institutos de pesquisa de sistemas elétricos e a fundação nacional de ciência dos Estados unidos \cite{SPMHO}. 
\par
As unidades de medição sincrofasorial, comumente conhecida como PMU, são dispositivos utilizados na medição da magnitude e fase de um fasor elétrico, como tensão ou corrente, utilizando uma fonte comum de tempo sincronizada.
\par
Os fasores elétricos tem sido objeto de estudo para os pesquisadores a muito tempo, pois tais informações são de particular importância desde a etapa de planejamento e despacho, até a operação do sistema em regime contínuo.

\subsection{Representação fasorial de uma senoide}
\par
O sinal elétrico senoidal puro pode ser representado matematicamente pela equação \ref{equation:pure_sin}:

\begin{equation}
    \label{equation:pure_sin}
    x(t)= R_{e}\left \{ X_{m}.e^{j(\omega.t+\phi)} \right \} = R_{e}\left [\left \{ e^{j(\omega.t)} \right \} X_{m}.e^{j.\phi} \right ] 
\end{equation}

\par
Onde $\omega$ é a frequência do sinal em radianos por segundo e $\phi$ o ângulo de fase em radianos. $X_{m}$ é a amplitude máxima do sinal. O valor médio quadrático, do inglês \criarsigla*{RMS}{\textit{Root Mean Square}} utilizado para calcular potências ativa e reativa em um sistema CA, é dado por $\frac{X_{m}}{\sqrt{2}}$.

\par
A representação do sinal senoidal na forma fasorial, a partir da equação \ref{equation:pure_sin} é dada pelo número complexo $\boldsymbol{X}$.

\begin{equation}
    \label{equation:pure_sin_fasor_X}
    x(t)\leftrightarrow X=(\frac{X_{m}}{\sqrt{2}})e^{j\theta }=(\frac{X_{m}}{\sqrt{2}})[cos\phi+jsen\phi]
\end{equation}

\par
Uma senoide e sua representação fasorial são apresentadas na figura \ref{image:senoid_phasor}.

\begin{figure} [!ht]
    \centering
    \caption{Senoide e seu fasor}
    \includegraphics[width=16cm]{senoid_phasor.png}
    Uma senoide \textbf{(a)} e sua representação na forma de fasor \textbf{(b)}
    \par FONTE: Adaptado de \textcite{SPMATA}
    \label{image:senoid_phasor}
\end{figure}  

\par
Embora a definição apresentada acima considere um sinal puro, porem na pratica, o sinal elétrico esta comumente contaminado por componentes em outras frequências. Uma das técnicas empregadas para efetuar o tratamento deste tipo de sinal é a transformada discreta de Fourier, do inglês \criarsigla*{DFT}{\textit{Discrete Fourier Transform}}, ou a transformada rápida re Fourier, do inglês \criarsigla*{FFT}{\textit{Fast Fourier Transform}}, das amostras do sinal que se deseja analisar. Além disso, a definição de fasor também implica que o sinal é imutável ao longo do tempo, entretanto, na pratica só é possível considerar o fasor para um intervalo de tempo determinado \cite{SPMATA}.
\par
Para efetuar a análise de um sinal, primeiramente é necessário efetuar o processo de amostragem do mesmo, porem, este processo requer filtragem do sinal, com a limitação da banda a uma frequência inferior à metade da frequência de amostragem aplicada. Isto é necessário para evitar a sobreposição das amostras no domínio da frequência, efeito que em inglês é conhecido como \textit{alias}, que poderia provocar distorção do sinal e erro nas medições. Tais filtros são conhecidos como \textit{anti-aliasing} e geralmente do tipo passa-baixa, analógicos ou digitais, dependendo dos requisitos da aplicação. A introdução de filtros nesta etapa provoca o deslocamento de fase do sinal original, que deve ser compensado para a correta determinação dos fasores. Considerando a utilização de $\boldsymbol{N}$ amostras de um sinal $x(t)$ na frequência nominal $f_{0}$ de um SEP, tais que $x_{k}{k=0, 1, ..., N-1}$, o fasor é dado por:

\begin{equation}
    \label{equation:sin_fasor}
    x(t)X_{N}=\frac{\sqrt{2}}{N}\sum_{k=0}^{N-1}x_{k}e^{-jk\frac{2\pi}{N}}
\end{equation}

\subsection{Arquitetura de uma PMU}
\par
Uma vez encontrado o método de cálculo do fasor, é de suma importância que cada fasor estimado esteja relacionado a uma estampa de tempo, que será utilizada para garantir que amostras de sinais que fazem parte de um mesmo sistema possam ser comparadas de forma correspondente, mesmo que estas amostras sejam obtidas em duas localidades distantes. Este objetivo é alcançado através da sincronização de uma rede de medição via receptores de sinal de um sistema de posicionamento global, do ingles \criarsigla*{GPS}{\textit{Global Positioning System}}. Dependendo da solução, pode ser utilizado um sinal da instalação ou da PMU. O sinal fasorial sincronizado resultante é denominado sincrofasor. A figura \ref{image:arck_pmu} ilustra a estrutura em blocos de uma PMU genérica \cite{SPMATA}.

\begin{figure} [!ht]
    \centering
    \caption{Arquitetura genérica de uma PMU}
    \includegraphics[width=16cm]{arquitetura_pmu.png}
    \par FONTE: Adaptado de \textcite{SPMATA}
    \label{image:arck_pmu}
\end{figure}  

Os sinais analógicos referenciados são as amostras de corrente e tensão disponíveis nos enrolamentos secundários de \criarsigla*{TPs}{Transformadores de Tensão} e \criarsigla*{TCs}{Transformadores de Corrente} existentes em subestações. São coletadas tensão e corrente trifásicas de forma que a medição de sequência positiva possa ser realizada. Após a filtragem \textit{anti-aliasing} um conversor analógico/digital transfere as informações processadas a um microprocessador onde os algoritmos de cálculo dos fasores são executados. O sinal de GPS é utilizado para sincronismo de fase do conversor A/D e do microprocessador, que aplica a estampa de tempo ao fasor calculado e então os dados podem ser transmitidos via modem a uma estação remota. Além das estimativas de sequência positiva de
tensões e corrente, ângulo de fase, frequência, taxa de variação de frequência, potências ativa e reativa são outras medidas tipicamente disponíveis em uma PMU \textcite{SPMATA}.

\subsection{Hierarquia do Sistemas de Medição Fasorial}

\par
As PMUs comumente são instaladas nas subestações dos SEPs e a localização destas subestação aonde estes dispositivos são instalados dependem do objetivo que se deseja atingir com as medições obtidas. O processo de definição do melhor posicionamento de uma PMU em SEPs é um assunto estudado por diversos pesquisadores como \textcite{PMUPTFCIO}, \textcite{APPSERT} e \textcite{AIMPAFNO}
\par
Na maior parte das aplicações, os dados obtidos através das PMUs são tratados em um local remoto em relação ao local aonde elas estão posicionadas, sendo assim, uma infraestrutura envolvendo as PMUs, links de comunicação, e concentradores de dados deve existir com o objetivo de melhorar o desempenho do sistema de monitoramento. A arquitetura geral de tal sistema é apresentada na figura \ref{image:gen_arck_pmu}.

\begin{figure} [!ht]
    \centering
    \caption{Hierarquia dos sistemas de medição fasorial e níveis de concentradores de dados fasoriais}
    \includegraphics[width=16cm]{arquitetura_geral_pmu.png}
    \par FONTE: Adaptado de \textcite{SPMATA}
    \label{image:gen_arck_pmu}
\end{figure} 

\par
A figura \ref{image:gen_arck_pmu} apresenta um cenário no qual as PMUs estão localizadas na subestação de um SEP e estão fornecendo dados de tensão, corrente e frequência de todos os barramentos monitorados. As medições são armazenadas em um dispositivo de armazenamento local, que pode ser acessado remotamente de qualquer lugar para analisar eventuais falhas no sistema ou para propósitos de diagnostico. A capacidade de armazenamento local é limitada, sendo assim o processo de armazenamento dos dados é rotativo, ou seja, dados antigos podem ser sobre-escritos por dados mais recentes, porem, os dados que correspondem a um evento relevante que ocorreu no SEP pode ser marcado para evitar que esta informação seja perdida.

\par
O dispositivo localizado no proximo nível da Hierarquia é comumente chamado de concentrador de dados fasoriais, do ingles \criarsigla*{PDCs}{\textit{Phasor Data Concentrators}}, é o dispositivo responsável por agregar os dados gerador por um conjunto de PMUs, rejeitar dados ruins, organizar os dados baseando-se em suas estampas de tempo e criar um conjunto de dados coerentes que representem o estado do sistema em tempo real. Os PDCs também podem ser utilizados para agregar dados armazenados em outros PDCs de forma a representar os dados gerador por um conjunto de PMUs em uma grande região do sistema que engloba mais de um PDC.

\subsection{Dados gerados}

\par
O primeiro padrão para sistemas de medição sincrofasorial publicado pelo IEEE foi o de número 1244-1995, com especificações a respeito da sincronização, precisão e amostragem de sinal. Não tratava ainda de requisitos de medição ou hierarquia de dados, incluídos quando da substituição pelo IEEE C37.118-2005 \cite{6111219}. Este, por sua vez, foi dividida em duas normas: IEEE Std C37.118.1-2011, com requisitos para medições e IEEE Std 27.118.2-2011, com os requisitos de comunicação de dados. Estas revisões foram superadas por duas normas, atualmente vigentes: IEC-IEEE 60255-118-1:2018, que versa sobre as medições e a IEEE C37.247\textsuperscript{TM}-2019, que estabelece os padrões para PDC. Conforme a figura \ref{image:structure_pmu}, os valores de entrada são a referência de tempo e as tensões e correntes do sistema de potência sob observação. O sinal de tempo deve ser um padrão UTC com precisão suficiente para o atendimento aos requisitos de sinais de entrada previstos pelo fabricante da PMU. As saídas serão os sincrofasores, frequência e a taxa de variação de frequência. Também podem estar disponíveis, valores calculados de potência ativa (MW), reativa (Mvar) e sinais discretos indicando estado lógico das PMUs, PDCs ou da instalação, conforme o caso. A norma IEEE Std C37.247\textsuperscript{TM}-2019 define requisitos funcionais para o alinhamento de tempo dos dados, transferência de dados, comunicação , formato de dados e conversão de coordenadas, conversão de taxa de informe de dados, ajuste de fase e monitoramento do sistema. Quanto a requisitos de desempenho esta norma trata de tempo de processamento do PDC, precisão de processamento de dados e robustez ante condições de operação degradada \cite{6111219}.

\begin{figure} [!ht]
    \centering
    \caption{Grandezas de entrada e saída de uma PMU}
    \includegraphics[width=16cm]{Estrutura_PMU.png}
    \par FONTE: Adaptado de \textcite{MAOSEPBISCAAM}
    \label{image:structure_pmu}
\end{figure} 


\section{Padronização de sinais} \label{section:Padronizacao_de_sinais}

\par
Na maior parte dos casos o conjunto de dados de entrada utilizados no processo de treinamento de um algoritmo de classificação irá conter dados com alta variabilidade de magnitude, unidade ou alcance e este tipo de variabilidade pode ser um problema para alguns tipos de algoritmo \cite{standard_distribution}. Se a variabilidade dos sinais não for considerada, os sinais com maior amplitude terão maior peso no processo de treinamento do que os sinais com menor amplitude, e esta diferença na característica dos sinais irá causar um desbalanceamento no processo de treinamento do algoritmo.

\par
Os métodos mais utilizados e discutidos de escalonamento são a normalização e a padronização. Neste trabalho somente o método de padronização será abordado. 

\par
O processo de padronização de sinais tem como objetivo re-dimensionar o sinal de forma que o mesma tenha as propriedades de uma distribuição padrão normal com $\mu=0$ and $\sigma=1$, aonde $\mu$ é a média e $\sigma$ é o desvio padrão em relação a média.

\begin{figure} [!ht]
    \centering
    \caption{Representação do processo de conversão de ums distribuição normal para uma padronizada}
    \includegraphics[width=16cm]{normal_standard_distribution.png}
    \par FONTE: Adaptado de \textcite{standard_distribution}
    \label{image:standard_distribution}
\end{figure} 

\par
O valor padronizado de um sinal para cada amostra é calculado através da equação \ref{equation:normalization}

\begin{equation}
    \label{equation:normalization}
    z=\frac{x-\mu}{\sigma}
\end{equation}

\par
O processo de padronização de um sinal para que sua média seja 0 e seu desvio padrão esteja proximo de 1 não é importante somente em situações aonde se é necessário comparar medições que possuem diferentes unidades, este tipo de pré-processamento é um pré-requisito para diversos tipos de algoritmos de otimização e algoritmos de aprendizado de maquina como SVM, perceptron, redes neurais e etc.


\section{Maquina de vetores de suporte} \label{section:SVM}

\par
A maquina de vetores de suporte foi inicialmente proposta por \textcite{TNSLT} e desde a sua divulgação gerou altissimo nível de interesse de toda a comunidade de pesquisa de assuntos relacionados a aprendizado de maquina.

As maquinas de vetores de suporte, do ingles \textit{Support Vector Machine}, são um conjunto de algoritmos de aprendizado supervisionado ou não supervisionado utilizadas comumente para problemas de classificação, regressão e detecção de anomalias que consistem no calculo de um hiperplano de separação equidistante aos conjuntos de dados presentes na etapa de treinamento do algoritmo de forma a minimizar o erro de classificação empírico e maximizar a margem de separação entre os conjuntos \cite{TNSLT}. 

Os dados de treinamento são representados como pontos no espaço, em que cada ponto pode ter N dimensões (características) e podem ser rotulado como pertencente a uma das classes. A figura \ref{image:hyperplano_svm} apresenta de forma visual um exemplo de hiper plano para um SVM treinado para separar dois conjuntos de dados.

\begin{figure} [!ht]
    \centering
    \caption{Margem maxima do hiper plano para um SVM treinado com amostras para 2 classes}
    \includegraphics[width=14cm]{imagens/SVMs-Optimize-Margin-Between-Support-Vectors-or-Classes.png}
    \par FONTE: Adaptado de \textcite{SVM_Introduction}
    \label{image:hyperplano_svm}
\end{figure} 

\par
Como visto na figura \ref{image:hyperplano_svm}, a margem refere-se à largura máxima da fatia que corre paralela ao hiperplano sem nenhum vetor de suporte interno. Tais hiperplanos são mais fáceis de definir para problemas linearmente separáveis; no entanto, para problemas ou cenários da vida real, muitas vezes os conjuntos de dados não podem ser linearmente separáveis, o que pode dar origem a classificações incorretas.

Uma das principais vantagens do SVM é a sua capacidade de lidar com dados que não são linearmente separáveis, utilizando técnicas como kernel trick. Isso permite que o algoritmo encontre soluções não lineares para problemas que não podem ser resolvidos por meio de algoritmos de classificação linear, como o perceptron.

Outra vantagem do SVM é sua capacidade de lidar com grandes conjuntos de dados de forma eficiente. O algoritmo seleciona apenas alguns dos pontos de dados de treinamento para definir o hiperplano de separação. Isso significa que o SVM é menos suscetível ao \textit{overfitting} quando comparado a outros algoritmos como árvores de decisão.

\par
Recentes estudos tem reportador que o algoritmo SVM é capaz de entregar altas performance em processos de classificação comparado com outros algoritmos de aprendizado de maquina \cite{DCUSVM}.

\par
O algoritmo SVM é baseado na teoria \criarsigla*{VC}{Vapnik–Chervonenkis} que explica o processo de aprendizado computacional por meio do ponto de vista estatístico. 

\par
Este método mapeia o vetor de entrada em um plano multi-dimensional, e cria uma superfície de separação em um ponto equidistante aos conjuntos apresentados pelos vetores de entrada. Para construir esta superfície de separação o método encontra duas superficies paralelas que sejam capazes de separa os dados, entretanto, estas superficies estarão posicionadas na fronteira dos conjuntos. A superfície de separação ótima é calculada a partir destas superficies paralelas em um ponto que maximize a distancia entre as superficies. De acordo com \textcite{TNSLT}, quanto maior for a distancia entre o plano de separação e as superficies localizadas na fronteira dos conjuntos, menor será o erro de classificação.

Existem vários tipos de kernels que podem ser usados no SVM, sendo os mais comuns o kernel linear, o polinomial e o RBF, do ingles \textit{Radial Basis Function}. Cada um desses kernels tem suas próprias propriedades e é adequado para diferentes tipos de problemas. A figura \ref{image:svm_kernel} apresenta de forma gráfica a diferença entre os diferentes tipos de kernel citados:

\begin{figure} [!ht]
    \centering
    \caption{Representação gráfica dos diferentes tipos de kernels que podem ser utilizados no algoritmo SVM}
    \includegraphics[width=16cm]{imagens/Kernel SVM.png}
    \par FONTE: O autor (2023)
    \label{image:svm_kernel}
\end{figure}

É importante notar que a escolha do kernel adequado depende do problema específico e da natureza dos dados. Em alguns casos, um kernel linear pode ser suficiente para obter uma boa separação entre as classes. Porém, se os dados são intrinsecamente não lineares, é mais provável que um kernel polinomial ou RBF seja necessário para alcançar uma boa performance de classificação.

\subsection{SVM para detecção de anomalias} \label{section:SVM_anomaly}

Uma das possíveis aplicações das maquinas de vetores de suporte são em problemas de detecção de anomalias, além de seu uso comumente conhecido em tarefas de classificação e regressão. A detecção de anomalias, é o processo de identificação de pontos de dados que se desviam significativamente da maioria dos dados. SVMs podem ser particularmente eficazes para esta tarefa devido à sua capacidade de criar um limite de decisão robusto. \cite{LIBSVM}

Na fase de treinamento, um SVM padrão é treinado usando os dados rotulados. No entanto, no contexto da detecção de anomalias, os dados não são rotulados, o que significa que não há classes específicas atribuídas aos pontos de dados. O objetivo é encontrar um hiperplano que melhor separe a maioria dos pontos de dados de possíveis anomalias. O SVM visa maximizar a margem, ou seja, a distância entre o hiperplano e os pontos de dados mais próximos.

Para resolver o problema de detecção de anomalias com dados não rotulados, uma variante do SVM chamada "\textit{One-Class SVM}" proposta por \textcite{one_svm} é frequentemente usada. O SVM de uma classe é treinado usando apenas dados normais, assumindo que os anomalias são raros e significativamente diferentes da maioria dos pontos de dados. Ele aprende a definir um limite que engloba os dados normais e visa maximizar a margem em torno dele. Isso permite identificar anomalias como pontos de dados fora do limite definido.

\textit{One-class SVM} é uma extensão do SVM para lidar com dados não rotulados. Como técnica madura para detecção de \textit{outliers}, o one-class SVM tem sido amplamente utilizado em muitas aplicações. 

Depois que o modelo SVM de uma classe é treinado, um limite pode ser definido para classificar os pontos de dados como normais ou discrepantes. A distância de um ponto de dados do limite de decisão pode ser usada como uma medida de sua anormalidade. Os pontos de dados que ultrapassam um determinado limite são classificados como anomalias.

Para um vetor de treino $x_i \in R^{n}$, i = 1, ..., $l$, where $l$ é o numero de observações e $n$ é o numero de características,  o problema de otimização resolvido pelo \textit{One-class SVM} é dado pela equação \ref{equation:SVM}:

\begin{equation}
    \label{equation:SVM}
    \begin{split}
        min_{\omega, \xi, \rho} \frac{1}{2} \omega^{T}\omega - \rho + \frac{1}{vl} \sum_{i=1}^{l} \xi_i 
        \\
        \textrm{subject to } \omega^{T}\phi (x_i) \ge \rho - \xi_i 
        \\
        \xi_i \ge 0, i=1, ..., l
    \end{split}
\end{equation}

Sendo, $v \in (0, 1)$ é um parâmetro e $\phi (x_i)$ é uma função para mapear $x_i$ em um vetor de dimensão superior. Para endereçar o problema do alta dimensionalidade de $\phi (x_i)$, é possivel propor a solução aonde $\omega$ de (1) é uma combinação linear de todos $\phi (x_i)$ com coeficiente $\alpha$:

\begin{equation}
    \label{equation:omega}
    \omega =  \sum_{i=1}^{l} \alpha_i \phi (x_i)
\end{equation}

Resolvendo as equações \ref{equation:SVM} e \ref{equation:omega}, a função de decisão do \textit{One-class SVM} é:

\begin{equation}
    \label{equation:SVMoneclass}
    \begin{split}
        f(x) = \omega^{T}\phi(x_i) - \rho
        \\
        =  \sum_{i=1}^{l} \alpha_i \phi(x_i)^{T} \phi(x) - \rho
    \end{split}
\end{equation}


\subsection{SVM multiclasse} \label{section:SVM_multiclass}

As maquinas de vetor de suporte são originalmente projetadas para tarefas de classificação binária, onde o objetivo é separar os pontos de dados em duas classes. No entanto, conforme apresentado por \textcite{Multiclass_SVM} as SVMs podem ser estendidas para lidar com problemas de classificação multiclasse por meio de várias técnicas. Uma abordagem comum é a estratégia "um contra todos" ou "um contra resto".

Na estratégia "um contra todos", um classificador SVM binário separado é treinado para cada classe, considerando-a como a classe positiva e as classes restantes como a classe negativa. Por exemplo, se houver N classes, N classificadores SVM binários serão treinados. Durante a previsão, cada classificador atribui um ponto de dados a uma das classes, e a classe com maior confiança ou probabilidade é selecionada como a classe prevista.

Durante a fase de treinamento, cada classificador SVM binário é treinado independentemente usando os dados rotulados. Os dados de treinamento para cada classificador consistem em amostras da classe positiva (a classe atual sendo considerada) e amostras de todas as outras classes combinadas como a classe negativa. O SVM aprende a encontrar um limite de decisão que melhor separe a classe positiva da classe negativa.

Para cada classificador binário, o SVM aprende uma função de decisão com base no kernel selecionado (por exemplo, linear, polinomial, RBF). Essa função de decisão pega os atributos de entrada e os mapeia para um espaço de dimensão superior, se necessário, aonde é realizada a separação entre as classes positivas e negativas. A função de decisão fornece uma medida de confiança ou probabilidade para cada classe.

Durante a previsão, o ponto de dados é passado por cada classificador binário. A função de decisão de cada classificador produz uma pontuação de confiança ou probabilidade para o ponto de dados pertencente à classe positiva. A classe com a pontuação de confiança mais alta é então selecionada como a classe predita para o ponto de dados.

A aplicação do SVM para classificação multiclasse nos permite estender seu uso além dos problemas binários. Ao treinar vários classificadores SVM binários e usar a estratégia um contra todos, podemos classificar efetivamente os pontos de dados em várias classes. De acordo com \textcite{Multiclass_SVM2} SVMs oferecem vantagens como a capacidade de lidar com dados não lineares por meio do uso de diferentes funções de kernel e o potencial para boa generalização e robustez em espaços de alta dimensão.


\section{Curva ROC e índice AUC} \label{section:ROC_AUC}
\par
A curva de características operacionais do receptor, do ingles \criarsigla*{ROC}{\textit{Receiver Operating Characteristic}}, é uma técnica utilizada para avaliar a capacidade de um classificador binário de discriminar entre duas classes, comparando a taxa de verdadeiros positivos (sensibilidade) com a taxa de falsos positivos (especificidade). 

A sensibilidade e a especificidade são dois conceitos importantes usados na avaliação de sistemas de  classificação. Esses conceitos medem o desempenho de um sistema em termos de sua capacidade de identificar corretamente os verdadeiros positivos e verdadeiros negativos. Aqui está uma explicação detalhada da diferença entre eles:

Sensibilidade é a capacidade de um sistema em detectar corretamente todos os casos positivos. É a proporção de verdadeiros positivos (casos corretamente identificados como positivos) em relação ao número total de casos positivos reais. Uma alta sensibilidade indica que o sistema é bom em identificar casos positivos, minimizando falsos negativos (casos positivos classificados erroneamente como negativos).

A especificidade é a capacidade de um sistema em detectar corretamente todos os casos negativos. É a proporção de verdadeiros negativos (casos corretamente identificados como negativos) em relação ao número total de casos negativos reais. Uma alta especificidade indica que o sistema é bom em identificar casos negativos, minimizando falsos positivos (casos negativos classificados erroneamente como positivos).

\par
A curva ROC é plotada com a taxa de verdadeiros positivos, do ingles \criarsigla*{TPR}{\textit{True positive rate}} no eixo Y e a taxa de falsos positivos, do ingles \criarsigla*{FPR}{\textit{False Positive Rate}} no eixo X. A TPR é a fração de casos positivos corretamente identificados pelo classificador, enquanto que a FPR é a fração de casos negativos que foram incorretamente identificados como positivos. A curva ROC é uma representação gráfica da relação entre a sensibilidade e a especificidade do classificador em diferentes pontos de corte.

\par
O processo de cálculo da curva ROC envolve a avaliação do classificador em diferentes pontos de corte. Para entender esse processo, é necessário compreender alguns conceitos importantes, como verdadeiros positivos (\criarsigla*{TP}{\textit{True Positive}}), falsos positivos (\criarsigla*{FP}{\textit{False Positive}}), verdadeiros negativos (\criarsigla*{TN}{\textit{True Negative}}) e falsos negativos (\criarsigla*{TP}{\textit{False Negative}}). TP são as instâncias que foram corretamente classificadas como pertencentes à classe positiva. FP são as instâncias que foram incorretamente classificadas como pertencentes à classe positiva, mas que na verdade pertencem à classe negativa. TN são as instâncias que foram corretamente classificadas como pertencentes à classe negativa, e FN são as instâncias que foram incorretamente classificadas como pertencentes à classe negativa. O calculo da taxa TPR e FPR são dadas pelas equações \ref{equation:TPR} e \ref{equation:FPR}:

\begin{equation}
    \label{equation:TPR}
    TPR = \frac{TP}{TP+FN} 
\end{equation}

\begin{equation}
    \label{equation:FPR}
    FPR = \frac{FP}{TN+FP} 
\end{equation}

\par
A área sob a curva, do ingles \criarsigla*{AUC}{\textit{Area Under the Curve}} é a área sob a curva ROC. Ele é um número que varia entre 0,5 e 1 e mede a capacidade geral do classificador em diferenciar entre as duas classes. Quanto maior o índice AUC, melhor é o desempenho do classificador. Valores próximos de 1 indicam que o classificador é muito bom na discriminação das duas classes, enquanto valores próximos de 0,5 indicam que o classificador não é capaz de discriminar entre as classes.

\par
A figura \ref{image:auc_roc} ilustra a relação entre a curva ROC e o índice AUC apresentado nesta sessão

\begin{figure} [!ht]
    \centering
    \caption{Índice AUC para diferentes curvas ROC}
    \includegraphics[width=12cm]{imagens/auc_roc.png}
    \par FONTE: O autor (2023).
    \label{image:auc_roc}
\end{figure} 


A seguir serão apresentados alguns gráficos relacionando a curva ROC com a distribuição dos conjuntos de dados que a geraram. Nestes exemplos, a curva de distribuição vermelha representa a classe positiva e a curva de distribuição verde representa a classe negativa.

\begin{figure} [!htb]
    \centering
    \caption{Curva ROC ideal}
    \includegraphics[width=16cm]{imagens/ROC_1.png}
    \par FONTE: \textcite{ROC_AUC}.
    \label{image:roc_1}
\end{figure} 


A figura \ref{image:roc_1} apresenta uma situação ideal. Quando duas curvas não se sobrepõem significa que o modelo possui uma medida ideal de separabilidade. Ele é perfeitamente capaz de distinguir entre a classe positiva e a classe negativa.

\begin{figure} [!htb]
    \centering
    \caption{Curva ROC com sobreposição das classes}
    \includegraphics[width=16cm]{imagens/ROC_0.7.png}
    \par FONTE: \textcite{ROC_AUC}.
    \label{image:roc_07}
\end{figure} 

Quando duas distribuições se sobrepõem, como apresentado na figura \ref{image:roc_07}, introduzimos erros do tipo 1 e do tipo 2. Dependendo do limiar, podemos minimizá-los ou maximizá-los. Quando a AUC é 0.7, significa que há uma chance de 70\% de o modelo conseguir distinguir entre a classe positiva e a classe negativa.

\begin{figure} [!htb]
    \centering
    \caption{Pior cenário da curva ROC}
    \includegraphics[width=16cm]{imagens/ROC_0.5.png}
    \par FONTE: \textcite{ROC_AUC}.
    \label{image:roc_05}
\end{figure} 

A figura \ref{image:roc_05} é a pior situação. Quando a AUC é aproximadamente 0.5, o modelo não possui capacidade de discriminação para distinguir entre a classe positiva e a classe negativa


\section{Considerações finais}

\par
Neste capítulo foram abordados os aspectos fundamentais deste trabalho, apresentando as ferramentas de simulação que foram utilizadas para o desenvolvimento deste trabalho, explorando os conceitos da padronização de sinais e sua importância no processo de treinamento de algoritmos de aprendizado de maquina e apresentando fundamentação teórica do método denominado SVM para classificação e detecção de anomalias em sinais. 

 