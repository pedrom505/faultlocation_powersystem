%% Same bar for all scenarios

clear
clc
close all

fault_type = 5;
bar_number = 30;
bar_list = (1:39);

voltage = [];
for bar = bar_list
    file_name = 'dataset/Bar_'+string(bar)+'-Fault_'+string(fault_type)+'.mat';
    load(file_name);

    voltage = [voltage simu_data.voltage_amplitude(:,bar_number)];
end

plot(voltage, 'b')

%% all bars for same scenario

clear
clc
close all

fault_type = 3;
bar = 3;

file_name = 'dataset/Bar_'+string(bar)+'-Fault_'+string(fault_type)+'.mat';
load(file_name);

voltage = simu_data.voltage_amplitude';
dim = size(voltage);

[X,Y] = meshgrid(1:dim(2),1:dim(1));

mesh(X,Y,voltage)




%% same line for all scenarios

clear
clc
close all

line_list = (1:46);
line_number = 5;
voltage = [];
for line = line_list
    file_name = 'dataset/Line_'+string(line)+'-Fault_6.mat';
    load(file_name);

    voltage = [voltage simu_data.voltage_amplitude(:,line_number)];
end

plot(voltage, 'b')

%% all lines for same scenarios
clear
clc
close all

file_name = 'dataset/NoFaults.mat';
load(file_name);

voltage = simu_data.voltage_amplitude';
dim = size(voltage);

[X, Y] = meshgrid(1:dim(2),1:dim(1));

mesh(X,Y,voltage)
