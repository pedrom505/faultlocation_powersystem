% bus data format
% bus: 
% col1 number
% col2 voltage magnitude(pu)
% col3 voltage angle(degree)
% col4 p_gen(pu)
% col5 q_gen(pu),
% col6 p_load(pu)
% col7 q_load(pu)
% col8 G shunt(pu)
% col9 B shunt(pu)
% col10 bus_type
%       bus_type - 1, swing bus
%               - 2, generator bus (PV bus)
%               - 3, load bus (PQ bus)
% col11 q_gen_max(pu)
% col12 q_gen_min(pu)
% col13 v_rated (kV)
% col14 v_max  pu
% col15 v_min  pu


system.sys_freq = 60;
system.basmva = 100;

system.bus = [...
1 1.03 18.5 7.00 1.61 0.00 0.00 0.00 0.00 1 5.0 -1.0 22.0 1.1 .9;
2 1.01 8.80 7.00 1.76 0.00 0.00 0.00 0.00 2 5.0 -1.0 22.0 1.1 .9;
3 0.9781 -6.1 0.00 0.00 0.00 0.00 0.00 3.00 3 0.0 0.0 230.0 1.5 .5;
4 0.95 -10 0.00 0.00 9.76 1.00 0.00 0.00 3 0.0 0.0 115.0 1.05 .95;
10 1.0103 12.1 0.00 0.00 0.00 0.00 0.00 0.00 3 0.0 0.0 230.0 1.5 .5;
11 1.03 -6.8 7.16 1.49 0.00 0.00 0.00 0.00 2 5.0 -1.0 22.0 1.1 .9;
12 1.01 -16.9 7.00 1.39 0.00 0.00 0.00 0.00 2 5.0 -1.0 22.0 1.1 .9;
13 0.9899 -31.8 0.00 0.00 0.00 0.00 0.00 5.00 3 0.0 0.0 230.0 1.5 .5;
14 0.95 -35 0.00 0.00 17.65 1.00 0.00 0.00 3 0.0 0.0 115.0 1.05 .95;
20 0.9876 2.1 0.00 0.00 0.00 0.00 0.00 0.00 3 0.0 0.0 230.0 1.5 .5;
101 1.00 -19.3 0.00 0.00 0.00 0.00 0.00 2.00 3 2.0 0.0 500.0 1.5 .5;
110 1.0125 -13.4 0.00 0.00 0.00 0.00 0.00 0.00 3 0.0 0.0 230.0 1.5 .5;
120 0.9938 -23.6 0.00 0.00 0.00 0.00 0.00 0.00 3 0.0 0.0 230.0 1.5 .5 ;
];

% line data format
% line: from bus, to bus, resistance(pu), reactance(pu),
%       line charging(pu), tap ratio, tap phase, tapmax, tapmin, tapsize

system.line = [...
1 10 0.0 0.0167 0.00 1.0 0. 0. 0. 0.;
2 20 0.0 0.0167 0.00 1.0 0. 0. 0. 0.;
3 4 0.0 0.005 0.00 1.0 0. 1.2 0.8 0.02;
3 20 0.001 0.0100 0.0175 1.0 0. 0. 0. 0.;
3 101 0.011 0.110 0.1925 1.0 0. 0. 0. 0.;
3 101 0.011 0.110 0.1925 1.0 0. 0. 0. 0.;
10 20 0.0025 0.025 0.0437 1.0 0. 0. 0. 0.;
11 110 0.0 0.0167 0.0 1.0 0. 0. 0. 0.;
12 120 0.0 0.0167 0.0 1.0 0. 0. 0. 0.;
13 101 0.011 0.11 0.1925 1.0 0. 0. 0. 0.;
13 101 0.011 0.11 0.1925 1.0 0. 0. 0. 0.;
13 14 0.0 0.005 0.00 1.0 0. 1.2 0.8 0.02;
13 120 0.001 0.01 0.0175 1.0 0. 0. 0. 0.;
110 120 0.0025 0.025 0.0437 1.0 0. 0. 0. 0.;
];


% Machine data format
% Machine data format
%       1. machine number,
%       2. bus number,
%       3. base mva,
%       4. leakage reactance x_l(pu),
%       5. resistance r_a(pu),
%       6. d-axis sychronous reactance x_d(pu),
%       7. d-axis transient reactance x'_d(pu),
%       8. d-axis subtransient reactance x"_d(pu),
%       9. d-axis open-circuit time constant T'_do(sec),
%      10. d-axis open-circuit subtransient time constant
%                T"_do(sec),
%      11. q-axis sychronous reactance x_q(pu),
%      12. q-axis transient reactance x'_q(pu),
%      13. q-axis subtransient reactance x"_q(pu),
%      14. q-axis open-circuit time constant T'_qo(sec),
%      15. q-axis open circuit subtransient time constant
%                T"_qo(sec),
%      16. inertia constant H(sec),
%      17. damping coefficient d_o(pu),
%      18. dampling coefficient d_1(pu),
%      19. type
%
% note: all the following machines use sub-transient model

system.mac_con = [ ...
1 1 900 0.200 0.00 1.8 0.30 0.25 8.00 0.03...
1.7 0.55 0.24 0.4 0.05...
6.5 0 0 1 0.0654 0.5743;
2 2 900 0.200 0.00 1.8 0.30 0.25 8.00 0.03...
1.7 0.55 0.25 0.4 0.05...
6.5 0 0 2 0.0654 0.5743;
3 11 900 0.200 0.00 1.8 0.30 0.25 8.00 0.03...
1.7 0.55 0.24 0.4 0.05...
6.5 0 0 3 0.0654 0.5743;
4 12 900 0.200 0.00 1.8 0.30 0.25 8.00 0.03...
1.7 0.55 0.25 0.4 0.05...
6.5 0 0 4 0.0654 0.5743;
];

system.exc_con = [...
0 1 0.01 200.0 0.05 0 0 5.0 -5.0...
0 0 0 0 0 0 0 0 0 0 0;
0 2 0.01 200.0 0.05 0 0 5.0 -5.0...
0 0 0 0 0 0 0 0 0 0 0;
0 3 0.01 200.0 0.05 0 0 5.0 -5.0...
0 0 0 0 0 0 0 0 0 0 0;
0 4 0.01 200.0 0.05 0 0 5.0 -5.0...
0 0 0 0 0 0 0 0 0 0 0;
];

system.tgt_con = [...
   1 .04 0.1 0.2 0.2 -0.5 0.025 3 0.1 0.1 -0.2 1000 0.42  4.2  0.1 0.7 0.25 0.25 0.5;
   2 .04 0.1 0.2 0.2 -0.5 0.025 3 0.1 0.1 -0.2 1000 0.42  4.2  0.1 0.7 0.25 0.25 0.5;
   3 .04 0.1 0.2 0.2 -0.5 0.025 3 0.1 0.1 -0.2 1000 0.42  4.2  0.1 0.7 0.25 0.25 0.5;
   4 .04 0.1 0.2 0.2 -0.5 0.025 3 0.1 0.1 -0.2 1000 0.42  4.2  0.1 0.7 0.25 0.25 0.5
];
%tgt_con = [];
system.pss_con = [...
1 1 100 10 0.05 0.015 0.08 0.01 0.2 -0.05;
1 2 100 10 0.05 0.015 0.08 0.01 0.2 -0.05;
1 3 100 10 0.05 0.015 0.08 0.01 0.2 -0.05;
1 4 100 10 0.05 0.015 0.08 0.01 0.2 -0.05;
];
% governor model
% tg_con matrix format
%column data unit
% 1 turbine model number (=1)
% 2 machine number
% 3 speed set point wf pu
% 4 steady state gain 1/R pu
% 5 maximum power order Tmax pu on generator base
% 6 servo time constant Ts sec
% 7 governor time constant Tc sec
% 8 transient gain time constant T3 sec
% 9 HP section time constant T4 sec
% 10 reheater time constant T5 sec
system.tg_con = [...
1 1 1 25.0 1.0 0.1 0.5 0.0 1.25 5.0;
1 2 1 25.0 1.0 0.1 0.5 0.0 1.25 5.0;
1 3 1 25.0 1.0 0.1 0.5 0.0 1.25 5.0;
1 4 1 25.0 1.0 0.1 0.5 0.0 1.25 5.0;
];

%pss_con = [];
system.load_con = [...
   4 0 0 0 0;
  14 0 0 0 0;
  ];
system.lmod_con = [...
  1 4 100 1 -1 1 .01; 
 2 14 100 1 -1 1 .01; 
];

system.rlmod_con = [...
 1 4 100 1 -1 1 .01; 
 2 14 100 1 -1 1 .01; 
];
%svc
% col 1           svc number
% col 2           bus number
% col 3           svc base MVA
% col 4           maximum susceptance Bcvmax(pu)
% col 5           minimum susceptance Bcvmin(pu)
% col 6           regulator gain
% col 7		      regulator time constant (s)
% col 8           lag time constant (Tb) seconds 
% col 9           lead time constant (Tc) seconds
% col 10          fraction of bus B pickup

system.svc_con = [...
    1    3   200   1.5  -1   10   0.05  0  0  0;
    2   13   200   1.5  -1   10   0.05  0  0  0;
    3  101   200   1.5  -1   10   0.05  0  0  0;
];
system.tcsc_con = [];
system.upfc_con = [];
system.hg_con=[];
% induction motor data
% 1. Motor Number
% 2. Bus Number
% 3. Motor MVA Base
% 4. rs pu
% 5. xs pu - stator leakage reactance
% 6. Xm pu - magnetizing reactance
% 7. rr pu 
% 8. xr pu - rotor leakage reactance
% 9. H  s  - motor plus load inertia constant
% 10. rr1 pu - second cage resistance
% 11. xr1 pu - intercage reactance
% 12. dbf    - deepbar factor
% 13. isat pu - saturation current
% 15. fraction of bus power drawn by motor 
system.ind_con = [ ...
  1  4   480  .03274 .08516 3.7788  .06164 .06005  1.0 0.01354 0.07517 0     3 0 .4;      
  2  14  960  .03274 .08516 3.7788  .06164 .06005  1.0 0.01354 0.07517 0     3 0 .4;
];
%ind_con = [];
% Motor Load Data 
% format for motor load data - mld_con
% 1 motor number
% 2 bus number
% 3 stiction load pu on motor base (f1)
% 4 stiction load coefficient (i1)
% 5 external load  pu on motor base(f2)
% 6 external load coefficient (i2)
% 
% load has the form
% tload = f1*slip^i1 + f2*(1-slip)^i2
system.mld_con = [ ...
1   4  0  1  .8   2;        
2  14  0  1  .8   2;
];
%mld_con = [];
% Motor Load Data 
% format for motor load data - mld_con
% 1 motor number
% 2 bus number
% 3 stiction load pu on motor base (f1)
% 4 stiction load coefficient (i1)
% 5 external load  pu on motor base(f2)
% 6 external load coefficient (i2)
% 
% load has the form
% tload = f1*slip^i1 + f2*(1-slip)^i2
system.uel_con=[];
system.gsib_idx = [];
system.geib_idx = [];

