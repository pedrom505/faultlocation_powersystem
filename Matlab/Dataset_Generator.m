clear;
clc;
close all;

d2asbegp_test;


[n_bus ,~] = size(system.bus);
[n_line ,~] = size(system.line);
BusSide_1 = system.line(:,1);
BusSide_2 = system.line(:,2);

noise = 0;

Selected_Buses = [1, 2, 3, 4, 10, 11, 12, 13, 14, 20, 101, 110, 120];

system.sw_con = [
    0       0   0   0   0   0   0.01;
    1       0   0   0   0   0   0.01;
    1.15    0   0   0   0   0   0.01;
    1.50    0   0   0   0   0   0.01;
    5.0     0   0   0   0   0   0
    ];

for fault_type = 0:5
    
    for index = 1:n_bus
        
        % From BusSide1 To BusSide2
        
        from_bus = BusSide_1(index);
        to_bus = BusSide_2(index);
        
        system.sw_con(2,2) = from_bus;
        system.sw_con(2,3) = to_bus;
        system.sw_con(2,6) = fault_type;
        
        filename = strcat(...
            'dataset/',...
            'FromBus_',num2str(from_bus),...
            '-ToBus_',num2str(to_bus),...
            '-Fault_',num2str(fault_type),...
            '.mat');
        
        if isfile(filename)
            disp(['File' filename ' already exists']);
        else
            output = PMU_Simulate(system, Selected_Buses, noise);
            save(filename,'output');
        end
        
        
        % From BusSide2 To BusSide1
        
        from_bus = BusSide_2(index);
        to_bus = BusSide_1(index);
        
        system.sw_con(2,2) = from_bus;
        system.sw_con(2,3) = to_bus;
        system.sw_con(2,6) = fault_type;
        
        filename = strcat(...
            'dataset/',...
            'FromBus_',num2str(from_bus),...
            '-ToBus_',num2str(to_bus),...
            '-Fault_',num2str(fault_type),...
            '.mat');
        
        if isfile(filename)
            disp(['File' filename ' already exists']);
        else
            output = PMU_Simulate(system, Selected_Buses, noise);
            save(filename,'output');
        end
    end
end
