%% IEEE 39-bus New England power system fault simulation for ANN dataset structuring
clear
clear global
clc
warning('off')



devider = [0.01 0.1 1 10 100 1000];

PS_Setup

better.RMSE = 1000;
better.mac_con =  PS_Data.mac_con;

for devider_3 = devider
    for devider_4 = devider
        for devider_6 = devider
            for devider_7 = devider
                for devider_8 = devider
                    for devider_11 = devider
                        for devider_12 = devider
                            for devider_13 = devider
                                for devider_16 = devider
                                    
                                    PS_Setup
                                    
                                    PS_Data.mac_con(:, 3) = PS_Data.mac_con(:, 3)/devider_3;
                                    PS_Data.mac_con(:, 4) = PS_Data.mac_con(:, 4)/devider_4;
                                    PS_Data.mac_con(:, 6) = PS_Data.mac_con(:, 6)/devider_6;
                                    PS_Data.mac_con(:, 7) = PS_Data.mac_con(:, 7)/devider_7;
                                    PS_Data.mac_con(:, 8) = PS_Data.mac_con(:, 8)/devider_8;
                                    PS_Data.mac_con(:, 11) = PS_Data.mac_con(:, 11)/devider_11;
                                    PS_Data.mac_con(:, 12) = PS_Data.mac_con(:, 12)/devider_12;
                                    PS_Data.mac_con(:, 13) = PS_Data.mac_con(:, 13)/devider_13;
                                    PS_Data.mac_con(:, 16) = PS_Data.mac_con(:, 16)/devider_16;
                                    
                                    % Identify number of bueses and lines in order to specify the number of simulations to be done.
                                    n_bus = length(PS_Data.bus);
                                    n_line = length(PS_Data.line);
                                    from_bus = PS_Data.line(:,1);
                                    to_bus = PS_Data.line(:,2);
                                    
                                    Noise = 0;
                                    PMU_Buses = (1:39);
                                    
                                    % Sweep through all possible cases.
                                    % No-fault simulation.
                                    
                                    PS_Data.sw_con(2,2) = from_bus(1); %Specify fault bus
                                    PS_Data.sw_con(2,3) = to_bus(1); %Specify far bus
                                    PS_Data.sw_con(2,6) = 6; % no fault
                                    
                                    %                                 filename = strcat('dataset/NoFaults.mat');
                                    % if isfile(filename)
                                    %     disp(['File' filename ' already exists']);
                                    % else
                                    simu_data = PMU_Simu(PS_Data, PMU_Buses, Noise);
                                    %                                 save(filename,'simu_data');
                                    % end
                                    
                                    voltage = simu_data.voltage_amplitude';
                                    
                                    mean_voltage = mean(voltage');
                                    
                                    delta_voltage = PS_Data.bus(:,2) - mean_voltage';
                                    
                                    RMSE = sqrt(mean(delta_voltage.^2));
                                    
                                    if RMSE < better.RMSE
                                        disp('--------------------------- BETTER CONDITION ---------------------------');
                                        better.RMSE = RMSE;
                                        disp('RMSE: '+ string(better.RMSE));
                                        better.mac_con =  PS_Data.mac_con;
                                        disp('------------------------------------------------------------------------');
                                        filename = strcat('dataset/better_value.mat');
                                        save(filename,'better');
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
dim = size(voltage);

[X, Y] = meshgrid(1:dim(2),1:dim(1));

mesh(X,Y,voltage)

%% IEEE 39-bus New England power system fault simulation for ANN dataset structuring
clear
clear global
clc
warning('off')


PS_Setup

PS_Data.mac_con(:, 16) = PS_Data.mac_con(:, 16);

% Identify number of bueses and lines in order to specify the number of simulations to be done.
n_bus = length(PS_Data.bus);
n_line = length(PS_Data.line);
from_bus = PS_Data.line(:,1);
to_bus = PS_Data.line(:,2);

Noise = 0;
PMU_Buses = (1:39);

% Sweep through all possible cases.
% No-fault simulation.

PS_Data.sw_con(2,2) = from_bus(1); %Specify fault bus
PS_Data.sw_con(2,3) = to_bus(1); %Specify far bus
PS_Data.sw_con(2,6) = 6; % no fault

%                                 filename = strcat('dataset/NoFaults.mat');
% if isfile(filename)
%     disp(['File' filename ' already exists']);
% else
simu_data = PMU_Simu(PS_Data, PMU_Buses, Noise);
%                                 save(filename,'simu_data');
% end

voltage = simu_data.voltage_amplitude';

mean_voltage = mean(voltage');           
delta_voltage = PS_Data.bus(:,2) - mean_voltage';
RMSE = sqrt(mean(delta_voltage.^2));
disp('RMSE: '+ string(RMSE));

dim = size(voltage);

[X, Y] = meshgrid(1:dim(2),1:dim(1));

mesh(X,Y,voltage)


%% Fault simulations.
for b_fault = 1:n_bus
    count_f = 0;
    count_t = 0;
    for numline = 1:n_line
        if ((from_bus(numline) == b_fault) && (count_f == 0))
            
            count_f = 1 + count_f;
            for f_type = 0:5
                % PS_Setup;
                
                PS_Data.sw_con(2,2) = b_fault;
                PS_Data.sw_con(2,3) = to_bus(numline);
                PS_Data.sw_con(2,6) = f_type;
                
                filename = strcat('dataset/Bar_',num2str(b_fault),'-Fault_',num2str(f_type),'.mat');
                
                if isfile(filename)
                    disp(['File' filename ' already exists']);
                else
                    simu_data = PMU_Simu(PS_Data, PMU_Buses, Noise);
                    save(filename,'simu_data');
                end
            end
        elseif ((to_bus(numline) == b_fault) && (count_t == 0))
            count_t = 1 + count_t;
            for f_type = 0:5
                % PS_Setup;
                
                PS_Data.sw_con(2,2) = b_fault;
                PS_Data.sw_con(2,3) = from_bus(numline);
                PS_Data.sw_con(2,6) = f_type;
                
                filename = strcat('dataset/Bar_',num2str(b_fault),'-Fault_',num2str(f_type),'.mat');
                if isfile(filename)
                    disp(['File' filename ' already exists']);
                else
                    simu_data = PMU_Simu(PS_Data, PMU_Buses, Noise);
                    save(filename,'simu_data');
                end
            end
        end
    end
end



%% Data organization, Part I - Scrambling the dataset in blocks of W samples.
% clear
% clc
%
% mat = dir('dataset/Caso*.mat'); %Load all .mat files corresponding to all simulations
%
% v_bus = [];
% i_bus = [];
% for index = 1:length(mat) %Structure the variable matrixes so that all simulations fit into a single (sample numbers)x15 matrix
%     mat_loaded = load([mat(index).folder '\' mat(index).name]);
%     v_bus = [v_bus; mat_loaded.v_bus];
%     i_bus = [i_bus; mat_loaded.i_bus];
% end
%
% data_size = size(v_bus);
% last_col = data_size(2);
%
% v_b = v_bus(:,(1:last_col-4)); %Values-only matrix
% i_b = i_bus(:,(1:last_col-4));
%
% v_idx = v_bus(:,(last_col-3:last_col)); %Fault type array
% i_idx = i_bus(:,(last_col-3:last_col));
%
%
% %Calculate the frequency of the PMU buses. Nominal frequency is 1 pu.
% mod_v = abs(v_b);
% ang_v = angle(v_b);
% mod_i = abs(i_b);
% ang_i = angle(i_b);
%
% freq_size = size(ang_v);
% freq = ones(freq_size);
% freq(2:end,:) = 1 + diff(ang_v); % The first value is the unchanged nominal frequency.
%
% %Convert rectangular to polar form so that values are more appropriate to a
% %[0 1] interval upon normalization
% mod_v = (mod_v - min(mod_v,[],'all'))/(max(mod_v,[],'all') - min(mod_v,[],'all'));
% ang_v = (ang_v - min(ang_v,[],'all'))/(max(ang_v,[],'all') - min(ang_v,[],'all'));
%
% freq =  (freq - min(freq,[],'all'))/(max(freq,[],'all') - min(freq,[],'all'));
%
% mod_i = (mod_i - min(mod_v,[],'all'))/(max(mod_i,[],'all') - min(mod_i,[],'all'));
% ang_i = (ang_i - min(ang_i,[],'all'))/(max(ang_i,[],'all') - min(ang_i,[],'all'));
%
%
% % Dataset without previous instant in consideration.
% dataset.voltage_amplitude = mod_v;
% dataset.voltage_angle = ang_v;
% dataset.current_amplitude = mod_i;
% dataset.current_angle = ang_i;
% dataset.frequency = freq;
% dataset.type_index = v_idx;
%
% filename = strcat('dataset_PS');
% save(filename, 'dataset');