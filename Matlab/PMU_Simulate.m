function simu_data = PMU_Simulate(System_data, PMU_Buses, Noise)

bus_index = [];
for sel_bus = PMU_Buses
    bus_index = [bus_index find(System_data.bus(:,1) == sel_bus)];
end

Output = System_simulate(System_data); % Fault simulation script

simu_data.time = Output.time;

% Calculating voltage
v_bus = Output.bus_v(bus_index,:);
if Noise ~=  0
    v_bus = awgn(v_bus, Noise, 'measured');
end

simu_data.voltage_complex = v_bus;
simu_data.voltage_amplitude = abs(v_bus);
simu_data.voltage_angle = angle(v_bus);

% Calculating current
i_bus = [];
for sel_bus = PMU_Buses
    i_bus = [i_bus; sum( Output.ilf(find(System_data.line(:,1) == sel_bus)',:), 1) + sum( Output.ilt(find(System_data.line(:,2) == sel_bus)',:), 1)];
end
if Noise ~= 0
    i_bus = awgn(i_bus, Noise, 'measured');
end

simu_data.current_complex = i_bus;
simu_data.current_amplitude = abs(i_bus);
simu_data.current_angle = angle(i_bus);

% Calculating frequency
freq_size = size(simu_data.voltage_angle);
freq = ones(freq_size);
freq(:,2:end) = 1 + diff(simu_data.voltage_angle')';
simu_data.frequency = freq;

end

