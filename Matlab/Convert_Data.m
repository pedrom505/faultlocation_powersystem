clear
close all

load('dataset_PS.mat')


n_samples = 480;
scenario_size = 39;

dim_input = size(dataset.type_index);

n_sample_set = dim_input(1)/n_samples;

for scenario = 0:3
    dirname = '../dataset/Scenario' + string(scenario);
    if not(isfolder (dirname))
        status = mkdir(dirname);
    end
    
    for sample_set = 0:n_sample_set-1
        
        begin_col=scenario*scenario_size+1;
        end_col=begin_col+scenario_size-1;
        
        begin_row = n_samples*sample_set+1;
        end_row = begin_row+n_samples-1;
        
        mat = targets(begin_row:end_row,scenario+1)';
        
        for index=begin_col:end_col
            mat = [mat ;inputs(begin_row:end_row,index)'];
        end
        
        csvwrite('../dataset/Scenario' + string(scenario) + '/Sample ' + string(sample_set) + '.csv', mat);
    end
end
