clear;
clc;
close all;

d2asbegp_test;

noise = 0;


system.sw_con = [
    0       0   0     0     0   0   0.01;
    1       3   101   0     0   6   0.01;  
    1.15    0   0     0     0   0   0.01;
    1.50    0   0     0     0   0   0.01;
    5.0     0   0     0     0   0   0
];

output = PMU_Simulate(system, [1, 2, 3, 4, 10, 11, 12, 13, 14, 20, 101, 110, 120], noise);

plotutil = PlotUtil;
PlotUtil.PlotSystem(system, output);