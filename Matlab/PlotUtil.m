classdef PlotUtil
    %UTIL Summary of this class goes here
    %   Detailed explanation goes here

    methods(Static)
        function PlotAllBuses(Time, Buses, Signal, SignalName)
            
            [nbus,~] = size(Signal);
            
            mesh(Time, (1:1:nbus), Signal)
            title([SignalName ' in all buses'])
            xlabel('time in seconds')
            ylabel('bus number')
            zlabel([SignalName ' in pu'])
            set(gca,'YTick' ,(1:1:nbus))
            set(gca,'YTickLabel' ,Buses)
        
        end
        
        function PlotSystem(System, SystemOutput)
        
            Buses = System.bus(: ,1)';
            
            figure;

            subplot(2,2,1);
            PlotUtil.PlotAllBuses(SystemOutput.time, Buses, SystemOutput.voltage_amplitude, 'Voltage');
            subplot(2,2,2);
            PlotUtil.PlotAllBuses(SystemOutput.time, Buses, SystemOutput.current_amplitude, 'Current');
            subplot(2,2,[3,4]);
            PlotUtil.PlotAllBuses(SystemOutput.time, Buses, SystemOutput.frequency, 'Frequency');
            
        end

    end
end

