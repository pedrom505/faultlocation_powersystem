function simu_data = PMU_Simu(PS_Data, PMU_Buses, Noise, Noise_flag)

%% loading PS data
bus = PS_Data.bus;
line = PS_Data.line;
mac_con = PS_Data.mac_con;
exc_con1 = PS_Data.exc_con1;
exc_con = PS_Data.exc_con;
tgt_con1 = PS_Data.tgt_con1;
tgt_con = PS_Data.tgt_con;
gsib_idx = PS_Data.gsib_idx;
geib_idx = PS_Data.geib_idx;
load_con = PS_Data.load_con;
hgt_con = PS_Data.hgt_con;
svc_con = PS_Data.svc_con;
tcsc_con = PS_Data.tcsc_con;
upfc_con = PS_Data.upfc_con;
lmod_con = PS_Data.lmod_con;
rlmod_con = PS_Data.rlmod_con;
pss_con = PS_Data.pss_con;
dcl_con = PS_Data.dcl_con;
dcr_con = PS_Data.dcr_con; 
dci_con = PS_Data.dci_con;
dcld_con = PS_Data.dcld_con;
dcrc_con = PS_Data.dcrc_con;
dcic_con = PS_Data.dcic_con;
sw_con = PS_Data.sw_con;

num_samples = ceil(sw_con(5,1)/sw_con(1,7));    % Number of samples for each simulation
pre_samples = ceil(sw_con(2,1)/sw_con(1,7));    % Number of samples before the failure
post_samples = num_samples - pre_samples;       % Number of samples after the failure

save('nedata.mat'); %Save workspace in order to load it into s_simu

s_simu % Fault simulation script

delete('sim_fle.mat');
delete('nedata.mat');

simu_data.fault_label = [zeros(pre_samples, 1); ones(post_samples, 1)];
simu_data.fault_type = f_type;

% Calculating voltage
v_bus = bus_v(PMU_Buses,:);
v_bus = v_bus';
if Noise ~=  0
    v_bus = awgn(v_bus, Noise, 'measured');
end
simu_data.voltage_complex = v_bus;

mod_v = abs(v_bus);
ang_v = angle(v_bus);
mod_v = (mod_v - min(mod_v,[],'all'))/(max(mod_v,[],'all') - min(mod_v,[],'all'));
ang_v = (ang_v - min(ang_v,[],'all'))/(max(ang_v,[],'all') - min(ang_v,[],'all'));

simu_data.voltage_amplitude = mod_v;
simu_data.voltage_angle = ang_v;

% Calculating current
i_bus = [];
for sel_bus = PMU_Buses
    i_bus = [i_bus; sum(ilf(find(line(:,1) == sel_bus)',:), 1) + sum(ilt(find(line(:,2) == sel_bus)',:), 1)];
end
i_bus = i_bus';
if Noise ~= 0
    i_bus = awgn(i_bus, Noise, 'measured');
end

simu_data.current_complex = i_bus;

mod_i = abs(i_bus);
ang_i = angle(i_bus);
mod_i = (mod_i - min(mod_v,[],'all'))/(max(mod_i,[],'all') - min(mod_i,[],'all'));
ang_i = (ang_i - min(ang_i,[],'all'))/(max(ang_i,[],'all') - min(ang_i,[],'all'));

simu_data.current_amplitude = mod_i;
simu_data.current_angle = ang_i;

% Calculating frequency
freq_size = size(ang_v);
freq = ones(freq_size);
freq(2:end,:) = 1 + diff(ang_v);
freq =  (freq - min(freq,[],'all'))/(max(freq,[],'all') - min(freq,[],'all'));

simu_data.frequency = freq;

end

