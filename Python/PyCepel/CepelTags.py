# Tags infos: first character is the length of the parameter and the second one is the number of empty spaces after the parameter
info = {
    'DEVT': {
        'Tp':       [4, 1],
        'Tempo':    [8, 0],
        'El':       [6, 0],
        'Pa':       [5, 0],
        'Nc':       [2, 0],
        'Ex':       [5, 1],
        '%':        [5, 1],
        'ABS':      [6, 1],
        'Gr':       [2, 1],
        'Und':      [3, 9],
        'Bl':       [4, 0],
        'P':        [1, 1],
        'Rc':       [6, 1],
        'Xc':       [6, 1],
        'Bc':       [6, 1],
        'Defas':    [7, 0]  
    }
}
