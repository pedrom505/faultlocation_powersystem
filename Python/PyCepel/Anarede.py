import os
import Logging
from PyCepel import CepelTags

class PWFFile:
    pwf_file_path = ''
    pwf_keys = {}

    def __init__(self, pwf_file_path: str) -> None:
        if os.path.isfile(pwf_file_path):
            self.pwf_file_path = pwf_file_path
            self._parser()
        else:
            raise Exception(f'File {pwf_file_path} was not found')


    def getLineInfo(self):
        content = '''   1         2 1      .35  4.11 69.87                                    
                        1        39 1       .1   2.5   75.                                    
                        2         3 1      .13  1.51 25.72                                    
                        2        25 1       .7   .86  14.6                                    
                        3         4 1      .13  2.13 22.14                                    
                        3        18 1      .11  1.33 21.38                                    
                        4         5 1      .08  1.28 13.42                                    
                        4        14 1      .08  1.29 13.82                                    
                        5         6 1      .02   .26  4.34                                    
                        5         8 1      .08  1.12 14.76                                    
                        6         7 1      .06   .92  11.3                                    
                        6        11 1      .07   .82 13.89                                    
                        7         8 1      .04   .46   7.8                                    
                        8         9 1      .23  3.63 38.04                                    
                        9        39 1       .1   2.5  120.                                    
                        10        11 1      .04   .43  7.29                                    
                        10        13 1      .04   .43  7.29                                    
                        13        14 1      .09  1.01 17.23                                    
                        14        15 1      .18  2.17  36.6                                    
                        15        16 1      .09   .94  17.1                                    
                        16        17 1      .07   .89 13.42                                    
                        16        19 1      .16  1.95  30.4                                    
                        16        21 1      .08  1.35 25.48                                    
                        16        24 1      .03   .59   6.8                                    
                        17        18 1      .07   .82 13.19                                    
                        17        27 1      .13  1.73 32.16                                    
                        21        22 1      .08   1.4 25.65                                    
                        22        23 1      .06   .96 18.46                                    
                        23        24 1      .22   3.5  36.1                                   
                        25        26 1      .32  3.23  51.3                                    
                        26        27 1      .14  1.47 23.96                                    
                        26        28 1      .43  4.74 78.02                                    
                        26        29 1      .57  6.25 102.9                                    
                        28        29 1      .14  1.51  24.9                                    
                        12        11 1      .16  4.35   0  1.006     
                        12        13 1      .16  4.35   0  1.006
                        6         31 1        0  2.50   0  1.070
                        10        32 1        0  2.00   0  1.070
                        19        33 1      .07  1.42   0  1.070
                        20        34 1      .09  1.80   0  1.009
                        22        35 1        0  1.43   0  1.025
                        23        36 1      .05  2.72   0  1.000
                        25        37 1      .06  2.32   0  1.025
                        2         30 1        0  1.81   0  1.025
                        29        38 1      .08  1.56   0  1.025
                        19        20 1      .07  1.38   0  1.060'''
        Lines_info = []
        content_splitted = content.split('\n')
        for line in content_splitted:
            line = " ".join(line.split())

            line_splitted = line.split(' ')
            if len(line_splitted) < 7:
                line_splitted.append(None)

            Lines_info.append({
                'De': line_splitted[0],
                'Pa': line_splitted[1],
                'c': line_splitted[2],
                'R': line_splitted[3],
                'X': line_splitted[4],
                'MVar': line_splitted[5],
                'Tap': line_splitted[6]
            })
            
        return Lines_info

    def _parser(self, ):
        print('')
