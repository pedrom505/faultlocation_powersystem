import os
import subprocess
import zlib
import Logging
from PyCepel import CepelTags



anatem_files = {
    "exe": "Anatem.exe"
}

class STBFile:

    def __init__(self, std_file_path: str) -> None:
        if os.path.isfile(std_file_path):
            self.std_file_path = ''
            self.stb_keys = {}
            self.std_file_path = std_file_path
            self._parser(std_file_path)
        else:
            raise Exception(f'File {std_file_path} was not found')

    def getParameter(self, param_name: str) -> dict:
        if param_name == 'ULOG':
            parameter = {
                '2': '39BUS_SYSTEM.HIS',
                '4': '39BUS.OUT',
                '8': '39BUS.PLT'
            }
            return parameter

    def setSystemEvent(self, event_type: str=None, event_time: float=None, 
    El: str=None, Pa: str=None, Nc: str=None, Ex: str=None, percent: str=None, 
    ABS: str=None, Gr: str=None, Und: str=None, Bl: str=None, P: str=None, 
    Rc: str=None, Xc: str=None, Bc: str=None, Defas: str=None):
    
        if 'DEVT' not in self.stb_keys:
            self.stb_keys['DEVT'] = []

        event = {
            'Tp': event_type,
            'Tempo': event_time,
            'El': El,
            'Pa': Pa,
            'Nc': Nc,
            'Ex': Ex,
            '%':  percent,
            'ABS': ABS,
            'Gr': Gr,
            'Und': Und,
            'Bl': Bl,
            'P': P,
            'Rc': Rc,
            'Xc': Xc,
            'Bc': Bc,
            'Defas': Defas 
        }

        self.stb_keys['DEVT'].append(event)

    def getPLTFilename(self) -> str:
        ulog_parameters = self.getParameter('ULOG')
        return ulog_parameters['8']

    def getPath(self) -> str:
        return self.std_file_path

    def save_as(self, file_name:str):
        file = open(f'{file_name}', "w")

        file_content = self._wrapper()
        file.write(file_content)

        file.close()

    def _wrapper(self) -> str:
        content = ''
        for key in self.stb_keys:
            content += f'{key}\n'
            for item in self.stb_keys[key]:
                for item_key in item:
                    
                    if item[item_key] is not None:
                        key_value = f'{item[item_key]}'
                    else:
                        key_value = ''

                    key_info = CepelTags.info[key][item_key]
                    max_size = key_info[0]
                    space_num = key_info[1]

                    content += f'{key_value.center(max_size)}' + ''.rjust(space_num)
                content += '\n'
            content += '999999\n'

        file = open(self.std_file_path, "r")

        aux_content = ''
        added_flag = False
        for line in file:
            aux_content += line
            if '999999' in line and not added_flag:
                added_flag = True
                aux_content += content
        
        return aux_content

    @staticmethod
    def _parser(std_file_path: str) -> dict:
        file = open(std_file_path, "r")
        file.close()

    @staticmethod
    def delete(file_path):
        Logging.info(f"Removing file {file_path}")
        os.remove(file_path)
        Logging.info(f"File {file_path} removed")

class PlotFile:

    def __init__(self, plt_file_path: str) -> None:
        Logging.info(f"Reading PLT file '{plt_file_path}'")
        if os.path.isfile(plt_file_path):
            if '.PLTZ' in plt_file_path:
                self._decompress(plt_file_path)
                plt_file_path = plt_file_path.replace('PLTZ', 'PLT')
                self.plt_file_path = plt_file_path
                self.content = self._unpack(plt_file_path)
                self._delete(plt_file_path)
            elif '.PLT' in plt_file_path:
                self.plt_file_path = plt_file_path
                self.content = self._unpack(plt_file_path)
            else:
                message = f"File '{plt_file_path}' is not compatible with PlotFile class"
                Logging.error(message)
                raise Exception(message)
        else:
            message = f"File '{plt_file_path}' was not found"
            Logging.error(message)
            raise Exception(message)
        Logging.info(f"File '{plt_file_path}' read successfully")

    def getContent(self) -> dict:
        return self.content
    
    def segmentContent(self, time_start, time_end=None):
        index = self.content['Tempo - segundos'].index(time_start)
        for key in self.content:
            self.content[key] = self.content[key][index:-1]
        self.content['Tempo - segundos'] = [round(time_stamp-time_start, 2) for time_stamp in self.content['Tempo - segundos']]

    def save(self, file_path: str = None, zip: bool = True):

        if file_path is None:
            file_path = self.plt_file_path

        file = open(file_path, "w")
        file_content = self._pack(self.content)
        file.write(file_content)
        file.close()

        if zip:
            self._compress(file_path)

    @staticmethod
    def _decompress(zip_file_path: str):
        Logging.debug(f"Unzipping file {zip_file_path}")
        if '.PLTZ' in zip_file_path:
            zip_file_name = os.path.basename(zip_file_path)
            zip_path = zip_file_path.replace(zip_file_name,'')
            file_name = zip_file_name.replace('.PLTZ', '.PLT')
            file_path = os.path.join(zip_path, file_name)

            file_in = open(zip_file_path, mode="rb")
            compressed_data = file_in.read()
            data = zlib.decompress(compressed_data)

            file_out = open(file_path, mode="wb")
            file_out.write(data)

            file_in.close()
            file_out.close()
            Logging.debug(f"File {zip_file_path} Unzipped")
        else:
            message = f"File {zip_file_path} is not a PLTZ file"
            Logging.error(message)
            raise Exception(message)

    @staticmethod
    def _compress(file_path: str):
        if '.PLT' in file_path:
            file_name = os.path.basename(file_path)
            zip_path = file_path.replace(file_name,'')
            zip_file_name = file_name.replace('.PLT', '.PLTZ')
            zip_path = os.path.join(zip_path, zip_file_name)

            file_in = open(file_path, mode="rb")
            data = file_in.read()
            compressed_data = zlib.compress(data, zlib.Z_BEST_SPEED)

            file_out = open(zip_path, mode="wb")
            file_out.write(compressed_data)

            file_in.close()
            file_out.close()

            os.remove(file_path)
        else:
            raise Exception(f'File {file_path} is not a PLT file')


    @staticmethod
    def _pack(content: dict) -> str:

        file_content = ''

        keys_list = list(content.keys())
        content_size = len(keys_list)

        file_content += f'   {str(content_size)}\n'

        for count, key in enumerate(list(content.keys())):

            if count == 0:
                file_content += f'{key}\n'
            else:
                [name, bar] = key.split(' ')
                name = name.ljust(13-len(bar))
                file_content += f'{name}{bar} BARRA-{int(bar):03d}\n'

        break_line_counter = 0
        signal_size = len(content[keys_list[0]])
        for counter in range(0, signal_size):
            for count, key in enumerate(list(content.keys())):
                value = "{:e}".format(content[key][counter])
                file_content += f' {value}'
                if break_line_counter > 4:
                    file_content += '\n'
                    break_line_counter = 0
                else:
                    break_line_counter += 1
            file_content += '\n'
            break_line_counter = 0

            
        return file_content

    @staticmethod
    def _unpack(plt_file_path: str) -> dict:
        Logging.info(f"Unpacking file '{plt_file_path}'")
        file = open(plt_file_path, "r")

        plt_file = {}

        number_lines = 0
        index_counter = 0
        for count, line in enumerate(file):
            line = line.replace('\n', '')
            if count == 0: 
                number_lines = int(line.replace(' ', ''))
            if count == 1:
                plt_file[line] = []
            if count > 1 and count <= number_lines:
                signal_name = " ".join(line.split()[0:2])
                plt_file[signal_name] = []
            if count > number_lines:
                for signal in line.split():

                    if index_counter >= number_lines:
                        index_counter = 0
                    
                    num_signal = float(signal)
                    dict_key = list(plt_file.keys())[index_counter]
                    plt_file[dict_key].append(num_signal)

                    index_counter += 1
        file.close()

        Logging.info(f"File '{plt_file_path}' unpackaged")
        return plt_file

    @staticmethod
    def _delete(file_path):
        Logging.info(f"Removing file {file_path}")
        os.remove(file_path)
        Logging.info(f"File {file_path} removed")


class Executer:

    def __init__(self) -> None:
        if self.isAnatemInstalled():
            self.anatem_exe = os.path.join(self.getAnatemPath(), anatem_files['exe'])
        else:
            raise Exception('Anatem is not installed in the computer')

    def simulate(self, stb_file: STBFile) -> PlotFile:

        stb_file_path = stb_file.getPath()

        workspace_path = stb_file_path.replace(os.path.basename(stb_file_path), '')
        
        proc = subprocess.run([self.anatem_exe,'/i' ,f'{stb_file_path}'], cwd=workspace_path, stdout=subprocess.PIPE)

        output = proc.stdout.decode('ISO-8859-1')
        if self.validateAnatemReport(output):
            plt_file_name = stb_file.getPLTFilename()
            plt_file_path = os.path.join(workspace_path, plt_file_name)

            plt_file = PlotFile(plt_file_path)

            return plt_file
        else:
            raise Exception('Anatem failure during the execution ')
        



    @staticmethod
    def getAnatemPath() -> str:
        for path in os.environ["PATH"].split(os.pathsep):
            if "Anatem" in path:
                return path
        raise Exception("Anatem was not find in this computer")

    @staticmethod
    def isAnatemInstalled() -> bool:
        for path in os.environ["PATH"].split(os.pathsep):
            if "Anatem" in path:
                return True
        return False

    @staticmethod
    def validateAnatemReport(anatem_report: str) -> bool:
        if "concluída com sucesso" in anatem_report:
            return True
        else:
            return False
        