import os
from PyCepel import Anatem, Anarede
import matplotlib.pyplot as plt
from matplotlib import cm
from Algorithm import Anomaly_detection, ShowUtil, TrainUtil
from sklearn import preprocessing
from matplotlib.ticker import LinearLocator
import numpy as np
import threading
import json
import random
import time
from itertools import product, combinations
from PIL import Image, ImageDraw, ImageFont
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn import svm
from sklearn.metrics import f1_score
from sklearn.neighbors import KNeighborsClassifier, NearestNeighbors
from sklearn.metrics import accuracy_score, confusion_matrix, roc_auc_score, roc_curve

# Inicializando sistema

json_file = open('database/database.json')
plt_content = json.load(json_file)
json_file.close()

bars = [*range(1, 40)]
timestamp = plt_content["timestamp"]
data_size = plt_content["data_size"]

# Calculando parâmetros

def calc_system_parameters(system_data):
    system_parameter = {}
    for bar in bars:
        voltage_normal = system_data['dataset']['normal'][f'VOLT {bar}']
        parameters = Anomaly_detection.calc_parameters([voltage_normal])
        system_parameter[f'BAR {bar}'] = parameters
    return system_parameter

system_parameter = calc_system_parameters(plt_content)

# Normalização por barra

def calc_normalization(system_data, system_parameter):
    results_dict = {}

    results_dict["timestamp"] = system_data["timestamp"]
    results_dict["data_size"] = system_data["data_size"]
    results_dict['dataset'] = {}

    dataset = system_data['dataset']

    # Calculating feature per bar
    for key in dataset:
        if 'normal' not in key:
            for bar in bars:
                voltage_fault = dataset[key][f'VOLT {bar}']
                sample = [voltage_fault]
                parameter = [system_parameter[f'BAR {bar}'][0]]
                if not key in results_dict['dataset']:
                    results_dict['dataset'][key] = {}
                results_dict['dataset'][key][f'VOLT {bar}'] = Anomaly_detection.calc_distance(sample, parameter)
    return results_dict

results_dict = calc_normalization(plt_content, system_parameter)


# Alocação de PMUs algoritmo detecção

while True:
    PMU_set = bars.copy()
    while len(PMU_set) >= 3:
        bar_score_list = []
        hash = random.getrandbits(128)
        for n_bar in PMU_set:
            PMU_new_set = PMU_set.copy()
            PMU_new_set.remove(n_bar)
            
            score = TrainUtil.estimate_system_accuracy_detection(results_dict, PMU_new_set)
            bar_score_list.append([n_bar, score])
            
        def sort_func(e):
            return e[1]

        bar_score_list.sort(key=sort_func)

        [worst_bar, score] = bar_score_list[-1]

        aux_bar_score_list = []
        for bar_score in bar_score_list:
            if bar_score[1] == score:
                aux_bar_score_list.append(bar_score)

        index = random.randint(0, len(aux_bar_score_list)-1)


        PMU_set.remove(aux_bar_score_list[index][0])

        f = open("Best_results_test_detection.txt", "a")
        f.write(f'|{hash}| - {PMU_set} - {score}\n')
        f.close()

