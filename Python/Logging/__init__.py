import logging
from logging import config
import inspect

config.fileConfig("log.config")
logging.getLogger("matplotlib").disabled = True
logging.getLogger("matplotlib.font_manager").disabled = True
logging.getLogger("matplotlib.pyplot").disabled = True
logging.getLogger("PIL.PngImagePlugin").disabled = True


def info(message: str):
    function_name = inspect.stack()[1][3]
    logger = logging.getLogger(function_name)
    logger.info(message)


def debug(message: str):
    function_name = inspect.stack()[1][3]
    logger = logging.getLogger(function_name)
    logger.debug(message)


def warning(message: str):
    function_name = inspect.stack()[1][3]
    logger = logging.getLogger(function_name)
    logger.warning(message)


def error(message: str):
    function_name = inspect.stack()[1][3]
    logger = logging.getLogger(function_name)
    logger.error(message)
