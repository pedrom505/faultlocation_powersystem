import os
from PyCepel import Anatem
import json

def validate_file_content(dataset, ref_timestamp):
    print('-- Validating file --')

    data_length = len(ref_timestamp)

    for key in dataset:
        if len(dataset[key]) != data_length:
            print(f'** key {key} is incompatible **')
            return False
    
    print('-- File approved --')
    return True
          
     

path = 'database/'
plt_content = {}

data_size = 0
timestamp = []

first_file = True


for (dirpath, dirnames, filenames) in os.walk(path):
    for filename in filenames:
        if '.PLTZ' in filename:
            print(f'Joining file {filename}')

            name = filename.replace('.PLTZ', '')
            name_splitted = name.split(' ')
            full_path = os.path.join(dirpath, filename)

            [system, fault_type, fault_removal] = name_splitted[0].split('_')
            [time, duration] = name_splitted[1].split('_')
            [from_bar, to_bar] = name_splitted[2].split('_')
            [Rc, Xc] = name_splitted[3].split('_')
            [LR, LQ] = name_splitted[4].split('_')

            pltfile =Anatem.PlotFile(full_path)
            dataset = pltfile.getContent()

            if first_file:
                timestamp = dataset['Tempo - segundos']
                data_size = len(timestamp)
                plt_content['timestamp'] = timestamp
                plt_content['data_size'] = data_size
                plt_content['dataset'] = {}
                first_file = False
            
            if validate_file_content(dataset, timestamp):
            
                key = f'{fault_type}_{fault_removal};{time}_{duration};{from_bar}_{to_bar};{Rc}_{Xc};{LR}_{LQ}'
                plt_content['dataset'][key] = {}

                for measure_type in dataset:
                    if 'VOLT' in measure_type:
                        plt_content['dataset'][key][measure_type] = dataset[measure_type][0:data_size]



json_object = json.dumps(plt_content)

full_path = os.path.join(path, 'database.json')
# Writing to sample.json
with open(full_path, "w") as outfile:
    outfile.write(json_object)