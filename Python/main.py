import matplotlib.pyplot as plt
from Algorithm import Anomaly_detection
from Algorithm import Util
import json


def read_dataset(path):
    json_file = open(path)
    plt_content = json.load(json_file)
    json_file.close()
    return  plt_content

plt_content = read_dataset('database/database.json')
bars = [*range(1, 40)]
timestamp = plt_content["timestamp"]
data_size = plt_content["data_size"]


def process_dataset (dataset, time_window):
    processed_dataset = {}
    max_time = round(max(timestamp))

    for simulation_name in dataset:

        if simulation_name not in processed_dataset:
            processed_dataset[simulation_name] = {}

        selected_simulation = plt_content['dataset'][simulation_name]
        [fault_type, fault_removal, from_bar, to_bar, time_start, time_end, Rc, Xc, LR, LQ] = Util.get_info(simulation_name)

        for key in selected_simulation:

            if key not in processed_dataset[simulation_name]:
                processed_dataset[simulation_name][key] = {}

            for time in range(0, max_time, time_window): 
                time_segment, data_segment = Util.segment_data(timestamp, selected_simulation[key], time, time+time_window)

                if time_start >= time and time_end <= time+time_window:
                    processed_dataset[simulation_name]['timestamp'] = time_segment
                    processed_dataset[simulation_name][key]['raw_data'] = data_segment
                    if 'parameter' in processed_dataset[simulation_name][key]:
                        processed_dataset[simulation_name][key]['previous_flag'] = True
                    else:
                        processed_dataset[simulation_name][key]['previous_flag'] = False
                else:
                    parameters = Anomaly_detection.calc_parameters([data_segment])
                    processed_dataset[simulation_name][key]['parameter'] = parameters

                if 'raw_data' in processed_dataset[simulation_name][key] and 'parameter' in processed_dataset[simulation_name][key]:
                    raw_data = processed_dataset[simulation_name][key]['raw_data']
                    parameter = processed_dataset[simulation_name][key]['parameter']
                    processed_data = Anomaly_detection.calc_distance([raw_data], parameter)
                    processed_dataset[simulation_name][key]['processed_data'] = processed_data

                    break

    return processed_dataset

processed_dataset = process_dataset(plt_content['dataset'], 10)