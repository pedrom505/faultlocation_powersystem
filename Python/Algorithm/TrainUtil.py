from sklearn.model_selection import train_test_split
import numpy as np
from sklearn import svm
from sklearn.metrics import f1_score
from Algorithm import Util
from sklearn.neighbors import KNeighborsClassifier, NearestNeighbors


def select_train_test_set(dataset, PMU_set):

    selected_dataset = []
    selected_labels = []

    for simulation_name in dataset:
        selected_simulation = dataset[simulation_name]
        timestamp = selected_simulation['timestamp']

        [fault_type, fault_removal, from_bar, to_bar, time_start, time_end, Rc, Xc, LR, LQ] = Util.get_info(simulation_name)

        data_array = []

        for key in selected_simulation:
            if key is not 'timestamp':
                bar = int(key.split(' ')[1])
                if bar in PMU_set:

                    processed_data = selected_simulation[key]['processed_data']

                    time_segment, data_segment = Util.segment_data(timestamp, processed_data, time_start+0.01, time_end+0.01)
                    data_array.append(data_segment)

        data_array = np.transpose(data_array)

        selected_dataset.extend(data_array)
        for count in range(0, len(data_array)):
            selected_labels.extend([int(from_bar)])

    train_data_list, test_data_list, train_label_list, test_label_list = train_test_split(selected_dataset, selected_labels)

    return [train_data_list, test_data_list, train_label_list, test_label_list]








def select_train_test_set_detection(results_data, PMU_set):

    dataset = results_data['dataset']
    data_size = results_data['data_size']
    
    selected_dataset = []
    selected_labels = []

    for key in dataset:

        [fault, from_bar, to_bar, index_start, index_end, Rc, Xc] = Util.get_info(key)

        [sur_index_start, sur_index_end] = Util.surroundings([index_start, index_end], 100, data_size)


        selected_samples = []
        for bar in PMU_set:
            data = dataset[key][f'VOLT {bar}']
            selected_samples.append(data[sur_index_start:sur_index_end].tolist())

        label = np.zeros(data_size, dtype=int)
        label[index_start:index_end] = 1
        label = label[sur_index_start:sur_index_end].tolist()

        trans_selected_samples = np.array(selected_samples).transpose()

        selected_dataset.extend(trans_selected_samples.tolist())
        selected_labels.extend(label)


    train_data_list, test_data_list, train_label_list, test_label_list = train_test_split(selected_dataset, selected_labels)

    return [train_data_list, test_data_list, train_label_list, test_label_list]



def estimate_system_accuracy_detection(results, PMU_set):
    folds = 2
    label_true = []
    label_pred = []
    for fold in range(folds):
        train_data_list, test_data_list, train_label_list, test_label_list = select_train_test_set_detection(results, PMU_set)

        knn = KNeighborsClassifier(n_neighbors=2)
        knn.fit(train_data_list, train_label_list)
        result_label_list = knn.predict(test_data_list)

        label_true.extend(test_label_list)
        label_pred.extend(result_label_list)

        
    score = f1_score(label_true, label_pred, average='micro')
    return score

def estimate_system_accuracy_location(results, PMU_set):
    folds = 6
    label_true = []
    label_pred = []
    for fold in range(folds):
        train_data_list, test_data_list, train_label_list, test_label_list = select_train_test_set(results, PMU_set)

        clf = svm.SVC(kernel='linear', decision_function_shape='ovo')

        clf.fit(train_data_list, train_label_list)
        result_label_list = clf.predict(test_data_list)

        label_true.extend(test_label_list)
        label_pred.extend(result_label_list)

        
    score = f1_score(label_true, label_pred, average='micro')
    return score


def estimate_system_prob_class(results, PMU_set):

    train_data_list, test_data_list, train_label_list, test_label_list = select_train_test_set(results, PMU_set)

    clf = svm.SVC(kernel='linear', decision_function_shape='ovo', probability=True)

    clf.fit(train_data_list, train_label_list)
    result_proba_list = clf.predict_proba(test_data_list)

    test_label_set = list(set(test_label_list))

    return test_label_set, result_proba_list*100, test_label_list