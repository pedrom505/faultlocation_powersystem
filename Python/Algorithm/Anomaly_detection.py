import numpy as np

def calc_parameters(dataset_list: list) -> list:

    parameters = []
    for dataset in dataset_list:

        dataset_array = np.array(dataset)

        average = 1/len(dataset_array)*sum(dataset_array)
        std_deviation = np.sqrt(1/len(dataset_array)*sum((dataset_array - average)**2))

        parameters.append((average, std_deviation))

    return parameters
    
def calc_probability(values: list, parameters: tuple) -> list:

    distribution = np.ones(len(values[0]))
    if len(values) == len(parameters):
        for value, parameter in zip (values, parameters):
            average = parameter[0]
            std_deviation = parameter[1]
            calc_dist = 1/np.sqrt(2*np.pi*std_deviation)*np.exp(-np.square(value-average)/(2*std_deviation**2))
            distribution *= calc_dist

        return distribution
    else:
        raise ValueError("The number of values isn't same numero of parameters")

def calc_distance(values: list, parameters: tuple) -> list:

    distance = np.zeros(len(values[0]))
    if len(values) == len(parameters):
        for value, parameter in zip (values, parameters):
            average = parameter[0]
            std_deviation = parameter[1]

            distance += (np.absolute(value - average)/std_deviation)**2

        distance = np.sqrt(distance)

        return distance
    else:
        raise ValueError("The number of values isn't same numero of parameters")

    