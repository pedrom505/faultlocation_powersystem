import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from Algorithm import Util


def get_perfil_data(file_name, data, window_position):

    time = data['Tempo - segundos']

    bar_list = []
    data_array = []

    for bar in data:
        if bar != 'Tempo - segundos':
            time = data['Tempo - segundos']
            current_data = data[bar]
            [time, selected_data] = Util.segment_data(time, current_data, window_position, window_position+0.01)
            data_array.append(selected_data)
            bar_list.append(bar.replace('VOLT ', ''))

    X_axis_values = bar_list
    Y_axis_values = data_array
    [fault_type, fault_removal, from_bar, to_bar, start_time, end_time, Rc, Xc, LR, LQ] = Util.parse_name(file_name)
    title = f'{fault_type}-{fault_removal} -> Fault bar {from_bar}/{to_bar}'

    return [title, X_axis_values, Y_axis_values]

def get_2d_data(file_name, data, selected_key = '', segment_period = []):

    time = data['Tempo - segundos']
    selected_data = data[selected_key]

    [time, selected_data] = Util.segment_data(time, selected_data, segment_period[0], segment_period[1])

    X_axis_values = time
    Y_axis_values = selected_data
    [fault_type, fault_removal, from_bar, to_bar, start_time, end_time, Rc, Xc, LR, LQ] = Util.parse_name(file_name)
    title = f'{fault_type}-{fault_removal} -> Fault bar {from_bar}/{to_bar}'

    return [title, X_axis_values, Y_axis_values]

def get_3d_data(file_name, data, segment_period = []):
    
    
    surf_data = []
    bars = []
    for bar in data:
        if bar != 'Tempo - segundos':
            time = data['Tempo - segundos']
            current_data = data[bar]
            [time, current_data] = Util.segment_data(time, current_data, segment_period[0], segment_period[1])

            samples = list(current_data)
            surf_data.append(samples)
            number = bar.split(' ')
            number = int(number[1])

            if number not in bars:
                bars.append(number)

    bars_mesh, index_mesh = np.meshgrid(time, bars)
    surf_data = np.array(surf_data)

    X_axis_values = bars_mesh
    Y_axis_values = index_mesh
    Z_axis_values = surf_data

    [fault_type, fault_removal, from_bar, to_bar, start_time, end_time, Rc, Xc, LR, LQ] = Util.parse_name(file_name)
    title = f'{fault_type}-{fault_removal} -> Fault bar {from_bar}/{to_bar}'

    return [title, X_axis_values, Y_axis_values, Z_axis_values]



def plot_3d(data, selected_key = None, segment_fault = False, max_plot = 10):
    plt.rcParams['figure.figsize'] = (19.2, 10.8)
    plt.rcParams.update({'font.size': 10})
    
    dataset = data['dataset']
    data_size = data['data_size']
    if selected_key:
        list_keys = selected_key
    else:
        list_keys = dataset.keys()

    count_plot = 0
    for key in list_keys:

        time_segmented = data['timestamp']

        [fault_type, fault_removal, from_bar, to_bar, index_start, index_end, Rc, Xc, LR, LQ] = Util.get_info(key)
        [index_start, index_end] = Util.surroundings([index_start, index_end], 100, data_size)

        if segment_fault:
            time_segmented = time_segmented[index_start:index_end]

        if count_plot > max_plot:
            break
        
        surf_data = []
        bars = []
        for bar in dataset[key]:
            if segment_fault:
                samples = list(dataset[key][bar][index_start:index_end])
            else:
                samples = list(dataset[key][bar])

            surf_data.append(samples)

            if bar not in bars:
                bars.append(int(bar.split(' ')[1]))

        bars_mesh, index_mesh = np.meshgrid(time_segmented, bars)
        surf_data = np.array(surf_data)

        fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
        plt.title(f"{key}", loc='left')
        surf = ax.plot_surface(bars_mesh, index_mesh, surf_data, cmap=cm.coolwarm, linewidth=0, antialiased=True)

        fig.colorbar(surf, shrink=0.5, aspect=5)

        plt.show()
        count_plot += 1