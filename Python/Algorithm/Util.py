import numpy as np

def parse_name(file_name: str):
    
    if '39BUS' in file_name and '.PLTZ' in file_name:
        file_name = file_name.replace('39BUS_', '')
        file_name = file_name.replace('.PLTZ', '')
        key_splitted = file_name.split(' ')
        if len(key_splitted) ==  5:

            [fault_type, fault_removal] = key_splitted[0].split('_')
            [start_time, duration] = key_splitted[1].split('_')
            [from_bar, to_bar] = key_splitted[2].split('_')
            [Rc, Xc] = key_splitted[3].split('_')
            [LR, LQ] = key_splitted[4].split('_')
            
            from_bar = int(from_bar)
            if to_bar != 'None':
                to_bar = int(to_bar)

            time_start = int(start_time)
            time_end = time_start + float(duration)

            Rc = float(Rc)
            Xc = float(Xc)
        else:
            [fault_type, fault_removal] = ['no_fault', 'no_fault']
            [time_start, time_end] = [0, 0]
            [from_bar, to_bar] = [0, 0]
            [Rc, Xc] = [0, 0]
            [LR, LQ] = [0, 0]
        
        return [fault_type, fault_removal, from_bar, to_bar, time_start, time_end, Rc, Xc, LR, LQ]

def get_info(identifier, separator = ';', sample_rate = 0.01):
    key_splitted = identifier.split(separator)
    if len(key_splitted) ==  5:

        [fault_type, fault_removal] = key_splitted[0].split('_')
        [start_time, duration] = key_splitted[1].split('_')
        [from_bar, to_bar] = key_splitted[2].split('_')
        [Rc, Xc] = key_splitted[3].split('_')
        [LR, LQ] = key_splitted[4].split('_')
        
        from_bar = int(from_bar)
        if to_bar != 'None':
            to_bar = int(to_bar)

        time_start = int(start_time)
        time_end = time_start + float(duration)

        index_start = int(time_start/sample_rate) + 1
        index_end = int(time_end/sample_rate) + 1

        Rc = float(Rc)
        Xc = float(Xc)

        return [fault_type, fault_removal, from_bar, to_bar, time_start, time_end, Rc, Xc, LR, LQ]
    else:
        print("Invalid identifier")


def segment_data(time, data, time_start, time_end):

    sample_rate = 0.01
    max_time = max(time)
    min_time = min(time)

    data = np.array(data)
    time = np.array(time)

    if time_start > time_end or time_end < time_start:
        time_vector = [time_start, time_end]
        time_start = min(time_vector)
        time_end = max(time_vector)

    if time_end > max_time:
        time_end = max_time
    if time_start < min_time:
        time_start = min_time

    mask = (time_start <= time) & (time < time_end)

    segmented_time = time[mask]
    segmented_data = data[mask]

    return segmented_time, segmented_data


def surroundings(index, neighborhood_size, data_size):
    increment = int(neighborhood_size/2)

    sel_start_index = index[0] - increment
    sel_end_index = index[1]  + increment

    if sel_start_index < 0:
        sel_end_index = sel_end_index - sel_start_index
        sel_start_index = 0
    elif sel_end_index > data_size:
        sel_start_index = sel_start_index - (sel_end_index - data_size)
        sel_end_index = data_size
    
    return [sel_start_index, sel_end_index]




