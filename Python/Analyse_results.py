import matplotlib.pyplot as plt
import numpy as np


type_result = 'detection'

file_obj = open(f"Best_results_test_location.txt", "r")
results = file_obj.readlines()

n_bars_list = []
perc_list = []
bars_list = []


for line in results:
    line = line.replace(' ', '')
    hash, bars, perc = line.split('-')
    perc = float(perc)

    bar_splitted = bars.replace('[', '').replace(']', '').split(',')

    for bar in bar_splitted:
        bars_list.append(int(bar))

    if len(bar_splitted) not in n_bars_list:
        n_bars_list.append(len(bar_splitted))
        perc_list.append([perc])
    else:
        index = n_bars_list.index(len(bar_splitted))
        perc_list[index].append(perc)

perl_mean = []
for perc in perc_list:
    perl_mean.append(np.mean(perc)*100)


result = []
for bar in range(1, 40):
    count = np.count_nonzero(np.array(bars_list) == bar)
    result.append([bar, count])

result.sort(key=lambda x:x[1])

result_transpose = np.transpose(result)

print(result_transpose.tolist())

fig1, ax1 = plt.subplots()
ax1.plot(n_bars_list, perl_mean)

ax1.set(xlabel='Numero de PMUs', ylabel='Precisão (%)',
       title='Precisão média x Numero de PMUs no sistema')
ax1.grid()
fig1.savefig(f"../Dissertacao/imagens/{type_result}_Accuracy x N_Pmus.png")

fig2, ax2 = plt.subplots()
ax2.hist(bars_list, 39, weights=100*np.ones(len(bars_list)) / len(bars_list))
ax2.set(xlabel='PMUs', ylabel='Relevancia (%)',
       title='Relevancia das barras para o sistema')
ax2.grid(True)

fig2.savefig(f"../Dissertacao/imagens/{type_result}_PMU_relevancia.png")

plt.show()










