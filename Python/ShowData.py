import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import os
from PyCepel import Anatem
from Algorithm import ShowUtil

folder_path = ''
file_name = ''
selected_data = None


# Função para gerar gráfico 3D
def generate_3d_graph(title, x_data, y_data, z_data):   
    fig = Figure(figsize=(9, 5), dpi=100)
    
    ax = fig.add_subplot(111, projection='3d')
    
    # Utilize os dados do arquivo externo para gerar o gráfico 3D
    # Exemplo de código para plotar um gráfico de pontos:

    surf = ax.plot_surface(x_data, y_data, z_data, cmap=cm.coolwarm, linewidth=0, antialiased=True)

    
    ax.set_xlabel('Tempo')
    ax.set_ylabel('Barras')
    ax.set_zlabel('Tensão (pu)')
    # ax.set_title(title)

    canvas = FigureCanvasTkAgg(fig, master=graph_area)
    canvas.draw()
    toolbar = NavigationToolbar2Tk(canvas, graph_area)
    toolbar.update()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

# Função para gerar gráfico 2D
def generate_2d_graph(title, x_data, y_data):
    fig = Figure(figsize=(9, 5), dpi=100)    
    ax = fig.add_subplot(111)
    
    # Utilize os dados do arquivo externo para gerar o gráfico 2D
    # Exemplo de código para plotar uma linha:

    ax.plot(x_data, y_data)
    ax.set_xlabel('Tempo')
    ax.set_ylabel('Tensão (pu)')
    # ax.set_title(title)

    canvas = FigureCanvasTkAgg(fig, master=graph_area)
    canvas.draw()
    toolbar = NavigationToolbar2Tk(canvas, graph_area)
    toolbar.update()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

# Função para gerar gráfico perfil
def perfil_graph(title, x_data, y_data):
    fig = Figure(figsize=(9, 5), dpi=100)    
    ax = fig.add_subplot(111)
    
    # Utilize os dados do arquivo externo para gerar o gráfico 2D
    # Exemplo de código para plotar uma linha:

    ax.plot(x_data, y_data)
    ax.set_xlabel('Tempo')
    ax.set_ylabel('Tensão (pu)')
    # ax.set_title(title)

    canvas = FigureCanvasTkAgg(fig, master=graph_area)
    canvas.draw()
    toolbar = NavigationToolbar2Tk(canvas, graph_area)
    toolbar.update()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

def refresh_UI():
    entry_window_size.delete(0, tk.END)



# Função para abrir um arquivo externo
def on_file_list_double_click(event):
    global selected_data
    global file_name

    file_name = file_list.selection_get()
    full_path = os.path.join(folder_path, file_name) 
    pltfile =Anatem.PlotFile(full_path)
    selected_data = pltfile.getContent()
    selected_file_label.config(text=file_name)

    refresh_UI()

    bar_list.delete(0, tk.END)
    file_scrollbar_bars.pack_forget()
    for key in selected_data:
        if key != 'Tempo - segundos':
            bar_list.insert(tk.END, key)
    
    bar_list.select_set(0)
    selected_bar_label.config(text=bar_list.selection_get())
    update_graph()

# Função chamada ao clicar na lista de barras
def on_bar_list_double_click(event):
    selected_bar_label.config(text=bar_list.selection_get()) 
    update_graph()


# Função para carregar uma pasta com arquivos
def load_folder():
    global folder_path 

    folder_path = filedialog.askdirectory()
    if folder_path:
        file_list.delete(0, tk.END)
        file_scrollbar_files.pack_forget()
        files = os.listdir(folder_path)
        for file in files:
            if "PLTZ" in file:
                file_list.insert(tk.END, file)
        
        file_list.select_set(0)

def get_start2end_time():

    time = selected_data['Tempo - segundos']
    window_position = scale_position.get()
    if entry_window_size.get() != '':
        window_size = float(entry_window_size.get())
    else:
        window_size = max(time)

    start_time = window_position - window_size/2
    end_time = window_position + window_size/2

    if start_time < 0:
        start_time = 0
    if end_time > max(time):
        end_time = max(time)

    return [start_time, window_position, end_time]


# Função para atualizar o gráfico com base no arquivo selecionado
def update_graph():
    clear_graph()
    [start_time, window_position ,end_time] = get_start2end_time()

    segment_period = [start_time, end_time]

    if graph_2d_var.get() == 1:
        selected_bar = selected_bar_label.cget('text')
        [title, x_data, y_data] = ShowUtil.get_2d_data(file_name, selected_data, selected_bar, segment_period)
        generate_2d_graph(title, x_data, y_data)

    elif graph_3d_var.get() == 1:
        [title, x_data, y_data, z_data] = ShowUtil.get_3d_data(file_name, selected_data, segment_period)
        generate_3d_graph(title, x_data, y_data, z_data)

    elif perfil_var.get() == 1:
        [title, x_data, y_data] = ShowUtil.get_perfil_data(file_name, selected_data, window_position)
        perfil_graph(title, x_data, y_data)


# Função para limpar o gráfico
def clear_graph():
    for widget in graph_area.winfo_children():
        widget.destroy()

# Função para atualizar a checkbox do gráfico 2D
def update_graph_2d():
    graph_3d_var.set(0)
    perfil_var.set(0)
    update_graph()


# Função para atualizar a checkbox do gráfico 3D
def update_graph_3d():
    graph_2d_var.set(0)
    perfil_var.set(0)
    update_graph()

def update_perfil():
    graph_2d_var.set(0)
    graph_3d_var.set(0)
    update_graph()

def update_scale(event):
    update_graph()



# Criando a janela da interface gráfica
root = tk.Tk()
root.title("Exemplo de Gráficos com Matplotlib")
root.geometry("1500x700")

# Checkbox para selecionar o tipo de gráfico (2D)
graph_2d_var = tk.IntVar(value=0)
graph_2d_checkbox = tk.Checkbutton(root, text="Gráfico 2D", variable=graph_2d_var, command=update_graph_2d)
graph_2d_checkbox.grid(row=0, column=0, padx=10, pady=10)

# Checkbox para selecionar o tipo de gráfico (3D)
graph_3d_var = tk.IntVar(value=1)
graph_3d_checkbox = tk.Checkbutton(root, text="Gráfico 3D", variable=graph_3d_var, command=update_graph_3d)
graph_3d_checkbox.grid(row=0, column=1, padx=10, pady=10)

# Checkbox para selecionar o tipo de perfil
perfil_var = tk.IntVar(value=0)
perfil_checkbox = tk.Checkbutton(root, text="Perfil", variable=perfil_var, command=update_perfil)
perfil_checkbox.grid(row=0, column=2, padx=10, pady=10)

# Botão para carregar pasta com arquivos
load_folder_button = tk.Button(root, text="Carregar Pasta", command=load_folder)
load_folder_button.grid(row=0, column=5, pady=10)


# Label para exibir a barra selecionada
selected_bar_label = tk.Label(root, text="")
selected_bar_label.grid(row=2, column=3, columnspan=2)

# Label para exibir o nome do arquivo selecionado
selected_file_label = tk.Label(root, text="")
selected_file_label.grid(row=2, column=5, columnspan=2)




# Área para exibir o gráfico
graph_area = tk.Frame(root, width=1000, height=400)
graph_area.grid(row=3, column=0, columnspan=3)

# Scrollbar para a lista de barras
file_scrollbar_bars = tk.Scrollbar(root)
file_scrollbar_bars.grid(row=3, column=4, sticky="NS")

# Lista de barras
bar_list = tk.Listbox(root, width=15, height=30, yscrollcommand=file_scrollbar_bars.set)
bar_list.grid(row=3, column=3, padx=10, pady=10)

# Configurar scrollbar para a lista de arquivos
file_scrollbar_bars.config(command=bar_list.yview)

# Associar evento de duplo clique na lista de arquivos
bar_list.bind("<Double-Button-1>", on_bar_list_double_click)


# Scrollbar para a lista de arquivos
file_scrollbar_files = tk.Scrollbar(root)
file_scrollbar_files.grid(row=3, column=6, sticky="NS")

# Lista de arquivos
file_list = tk.Listbox(root, width=50, height=30, yscrollcommand=file_scrollbar_files.set)
file_list.grid(row=3, column=5, padx=10, pady=10)

# Configurar scrollbar para a lista de arquivos
file_scrollbar_files.config(command=file_list.yview)

# Associar evento de duplo clique na lista de arquivos
file_list.bind("<Double-Button-1>", on_file_list_double_click)


# Configuração do scale para selecionar janela de visualização do grafico
scale_position_label = tk.Label(root, text="Posição de visuazação")
scale_position_label.grid(row=4, column=0)

var = tk.DoubleVar()
scale_position = tk.Scale( root, variable = var, digits=5, orient="horizontal", resolution=0.01, width = 20, length=350, from_=0.0, to=100.0, command=update_scale)
scale_position.grid(row=4, column=0, columnspan=3)


# Configuração do scale para selecionar janela de visualização do grafico
window_size_label = tk.Label(root, text="Tamanho da janela")
window_size_label.grid(row=5, column=0)

entry_window_size = tk.Entry(root, width = 10)
entry_window_size.grid(row=5, column=1)








# Iniciando o loop principal da interface gráfica
root.mainloop()
