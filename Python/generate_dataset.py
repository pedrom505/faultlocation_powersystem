from logging import exception
import os
from PyCepel import Anatem, Anarede
import json
import random

def no_event():
    anatem = Anatem.Executer()
    print(f'Generating case - no_fault')

    plt_file_name = f"39BUS.PLT"
    plt_saved_path = os.path.join('database', plt_file_name)

    if not os.path.exists(plt_saved_path+'Z'):
        stb_file_path = os.path.join('..', 'Anatem', '39bus_ANATEM.stb')
        stb_file_main = Anatem.STBFile(stb_file_path)
        plt_file = anatem.simulate(stb_file_main)
        plt_file.save(plt_saved_path)
        print(f'Case no_fault generated successfully')
    else:
        print(f'Case no_fault already exists')

def event(event_type, remover_type, bar_1, bar_2 = None, duration = 120):

    if event_type == 'APCB':
        percent = None
    elif event_type == 'APCL':
        percent = round(random.randint(0, 100)/100, 2)
    
    event_start = random.randint(25, duration-25)
    event_duration = round(random.randint(2, 5)/100, 2)

    Rc = round(random.randint(5, 200)/100, 2)
    Xc = round(random.randint(5, 200)/100, 2)

    load_variation_R = round(random.randint(-50, 50), 2)
    load_variation_Q = round(random.randint(-50, 50), 2)

    anatem = Anatem.Executer()
    suffix = f'{event_type}_{remover_type} {event_start-20}_{event_duration} {bar_1}_{bar_2} {Rc}_{Xc} {load_variation_R}_{load_variation_Q}'

    print(f'Generating case - {suffix}')

    plt_file_name = f"39BUS_{suffix}.PLT"
    plt_saved_path = os.path.join('database', plt_file_name)

    if not os.path.exists(plt_saved_path + 'Z'):
        stb_file_path = os.path.join('..', 'Anatem', '39bus_ANATEM.stb')
        stb_file_main = Anatem.STBFile(stb_file_path)

        for load_bar in [3, 4, 7, 8, 12, 15, 16, 18, 20, 21, 23, 24, 25, 26, 27, 28, 29, 31, 39]:
            stb_file_main.setSystemEvent(event_type='MDLP', event_time=0, El=load_bar,  Pa=None, percent=load_variation_R, Rc=None, Xc=None)
            stb_file_main.setSystemEvent(event_type='MDLQ', event_time=0, El=load_bar,  Pa=None, percent=load_variation_Q, Rc=None, Xc=None)
        
        
        stb_file_main.setSystemEvent(event_type=event_type, event_time=event_start, El=bar_1,  Pa=bar_2, percent=percent, Rc=Rc, Xc=Xc)
        stb_file_main.setSystemEvent(event_type=remover_type, event_time=event_start + event_duration, El=bar_1, Pa=bar_2)

        stb_new_file_path = os.path.join('..', 'Anatem', f'39bus_ANATEM_{suffix}.stb')
        stb_file_main.save_as(stb_new_file_path)

        stb_file_new = Anatem.STBFile(stb_new_file_path)
        try:   
            plt_file = anatem.simulate(stb_file_new)

            if len(plt_file.getContent()['Tempo - segundos']) < duration*100:
                print('Size of data lower then 9000')
                stb_file_new.delete(stb_new_file_path)
            else:
                plt_file.segmentContent(20)
                plt_file.save(plt_saved_path)
                stb_file_new.delete(stb_new_file_path)
                print(f'Case {suffix} generated successfully')
        except Exception as e:
            print(e)
            stb_file_new.delete(stb_new_file_path)
        
    else:
        print(f'Case {suffix} already exists')


# Generating dataset without fault
no_event()

# Generating dataset with fault
pwf_file_path = os.path.join('..', 'Anatem', '39bus_system.pwf')
pwf_file = Anarede.PWFFile(pwf_file_path)
Lines_info = pwf_file.getLineInfo()
for count in range(4):

    for line in Lines_info:
        from_bar = int(line['De'])
        to_bar = int(line['Pa'])
        tap = line['Tap']

        if tap is None:
            event('APCL', 'ABCI', from_bar, to_bar) # Abertura de linha aonde ocorreu a falta
            event('APCL', 'RMCL', from_bar, to_bar) # Remove a falta

    for bar in range(1, 40):
        event('APCB', 'RMCB', bar)


